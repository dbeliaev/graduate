<?php
include "../components/Autoload.php";
session_start();
Admin::Check();
 
include 'html_files/headhtml.php';
    ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Корзина</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Cart</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">

                                <table id="product" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Изображение</th>
                                        <th>Название</th>
                                        <th>Количество</th>
                                        <th>Админ описание</th>
                                        <th>Код</th>
                                        <th>Действия</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php

                                        $cart = Admin::cart_admin($_GET['id']);
                                        Admin::admin_cart_out($cart,$_GET['id']);

                                        ?>

                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="float-right d-none d-sm-block">
            <b>Version</b> 3.1.0-rc
        </div>
        <strong>Copyright &copy; 2014-2020 </strong> AdminLTE.io  All rights reserved.
    </footer>

    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<?php
include 'html_files/scripts.php'

?>



