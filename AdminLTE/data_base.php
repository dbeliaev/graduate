<?php
include "../components/Autoload.php";
session_start();
Admin::Check();

include 'html_files/headhtml.php';
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="min-height: 602px;">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Data Base</h1>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Main row -->
                <div class="row">
                    <div class="col-sm-6">
                    <div class="card card-primary">
                        <form action="../controls/add_to_bd.php" method="post">
                        <div class="card-header">
                            <h3 class="card-title">Добавление продукта</h3>
                        </div>
                        <!-- /.card-header -->

                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleSelectRounded0">Главная кнопка!!!</label>
                                <select name="header_cat" id="header_cat" class="custom-select rounded-0" onchange="selector('header_cat', this.value)">
                                    <?php
                                    $db =Db::getConnection();
                                    $sql =  $db->query('SELECT `id`, `name` FROM `header_cat` ');
                                    while ($result = $sql->fetch()) {
                                        if($result['id'] == '1' || $result['id'] == '2'|| $result['id'] == '3' ){
                                            echo "<option value='{$result['id']}'> {$result['name']}</option>";
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleSelectRounded0">Категории</label>
                                <select name="category" id="category" class="custom-select rounded-0" onchange="selector('category', this.value)">
                                    <?php
                                    $db = Db::getConnection();
                                    $sql =  $db->query('SELECT `id`, `name`, `header_cat_id` FROM `category` WHERE header_cat_id=1 ');
                                    while ($result = $sql->fetch()) {
                                        echo "<option value='{$result['id']}'> {$result['name']}</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleSelectRounded0">Подкатегории</label>
                                <select name= "subcategory" id="subcategory" class="custom-select rounded-0" onchange="selector('subcategory', this.value)">
                                    <?php
                                    $db = Db::getConnection();
                                    $sql =  $db->query('SELECT `id`, `name`, `category_id` FROM `subcategory` WHERE category_id=1 ');
                                    while ($result = $sql->fetch()) {
                                        echo "<option value= '{$result['id']}'> {$result['name']}</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleSelectRounded0">Крайняя категория</label>
                                <select name= "final_cat" id="final_cat" class="custom-select rounded-0">
                                    <?php
                                    $db = Db::getConnection();
                                    $sql =  $db->query('SELECT `id`, `name`, `subcategory` FROM `final_cat` WHERE subcategory=1 ');
                                    while ($result = $sql->fetch()) {
                                        echo "<option value='{$result['id']}'> {$result['name']}</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleSelectRounded0">Размер</label>
                                <select name= "size" class="custom-select rounded-0">
                                    <?php
                                    $db = Db::getConnection();
                                    $sql =  $db->query('SELECT `id`, `name` FROM `size` ');
                                    while ($result = $sql->fetch()) {
                                        echo "<option value='{$result['id']}'> {$result['name']}</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <h4>Характеристика</h4>
                                <!-- text input -->
                                <div class="form-group">
                                    <input name="name" type="text" class="form-control" placeholder="Название">
                                </div>
                            <div class="form-group">
                                <input name="availability" type="text" class="form-control" placeholder="Количество">
                            </div>
                            <div class="form-group">
                                <input name="id" type="text" class="form-control" placeholder="Код">
                            </div>
                                <div class="form-group">
                                    <input name="img" type="text" class="form-control" placeholder="Картинка">
                                </div>
                                <div class="form-group">
                                    <textarea name="dis" class="form-control" rows="3" placeholder="Описание"></textarea>
                                </div>
                            <div class="form-group">
                                <textarea name="dis_ad" class="form-control" rows="3" placeholder="Описание_админа"></textarea>
                            </div>
                            </div>
                           <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Добавить</button>
                            </div>
                        </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                    <!-- Left col -->
                </div>
                <!-- /.row -->
            </div><!--/. container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <strong>Copyright © 2014-2020 <a href="https://adminlte.io">AdminLTE.io</a>.</strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
            <b>Version</b> 3.1.0-rc
        </div>
    </footer>
    <div id="sidebar-overlay"></div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="plugins/raphael/raphael.min.js"></script>
<script src="plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard2.js"></script>
<script src="../template/admin/js/tsel.js"></script>
<script>selector('header_cat', header_cat.value)</script>
</body>
</html>

