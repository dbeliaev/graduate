<?php
include "../components/Autoload.php";
session_start();
Admin::Check();
$ed = Admin::edit_data($_GET['id']);
$hid= $ed['header_cat_id'];
$cid= $ed['category_id'];
$sid= $ed['subcategory_id'];
$fid= $ed['final_cat_id'];
$sizeid = $ed['size_id'];
$dis = $ed['description'];
$dis_ad = $ed['description_ad'];
$code = $ed['code'];
$name= $ed['name'];
$name = htmlspecialchars($name);
$av = $ed['availability'];
$img = $ed['img'];
include 'html_files/headhtml.php';
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 602px;">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Data Base</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Main row -->
            <div class="row">
                <div class="col-sm-6">
                    <div class="card card-primary">
                        <form action="../controls/ed_to_bd.php" method="post">
                            <div class="card-header">
                                <h3 class="card-title">Изменение продукта</h3>
                            </div>
                            <!-- /.card-header -->

                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleSelectRounded0">Главная кнопка!!!</label>
                                    <select name="header_cat" class="custom-select rounded-0" >
                                        <?php
                                        $db = Db::getConnection();
                                        $sql =  $db->query('SELECT id, name FROM header_cat');
                                        while ($result = $sql->fetch()) {
                                            if($result['id'] == '1' || $result['id'] == '2' || $result['id'] == '3'){
                                                if($result['id'] == $cid) $select = 'selected'; else $select = '';
                                                echo "<option value='{$result['id']}' $select> {$result['name']}</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleSelectRounded0">Категории</label>
                                    <select name="category" class="custom-select rounded-0">
                                        <?php
                                        $db = Db::getConnection();
                                        $sql =  $db->query('SELECT id, name, header_cat_id FROM category ');
                                        while ($result = $sql->fetch()) {
                                            if($result['id'] == $cid) $select = 'selected'; else $select = '';
                                            echo "<option value='{$result['id']}' $select> {$result['name']}</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleSelectRounded0">Подкатегории</label>
                                    <select name= "subcategory" class="custom-select rounded-0">
                                        <?php
                                        $db = Db::getConnection();
                                        $sql =  $db->query('SELECT id, name, category_id FROM subcategory');
                                        while ($result = $sql->fetch()) {
                                            if($result['id'] == $sid) $select = 'selected'; else $select = '';
                                            echo "<option value= '{$result['id']}' $select> {$result['name']}</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleSelectRounded0">Крайняя категория</label>
                                    <select name= "final_cat" class="custom-select rounded-0">
                                        <?php
                                            $db = Db::getConnection();
                                            $sql = $db->query('SELECT id, name, subcategory FROM final_cat');
                                            while ($result = $sql->fetch()) {
                                                if($result['id'] == $fid) $select = 'selected'; else $select = '';
                                                echo "<option value='{$result['id']}' $select> {$result['name']}</option>";
                                            }

                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleSelectRounded0">Размер</label>
                                    <select name= "size" class="custom-select rounded-0">
                                        <?php
                                        $db = Db::getConnection();
                                        $sql =  $db->query('SELECT id, name FROM size ');
                                        while ($result = $sql->fetch()) {
											if($result['id'] == $sizeid) $select = 'selected'; else $select = '';
                                            echo "<option value='{$result['id']}' $select> {$result['name']}</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <h4>Характеристика</h4>
                                <!-- text input -->
                                <div class="form-group">
                                    <input name="name" type="text" class="form-control" value="<?php echo $name;?>" >
                                </div>
                                <div class="form-group">
                                    <input name="availability" type="text" class="form-control" value="<?php echo $av;?>">
                                </div>
                                <div class="form-group">
                                    <input name="id" type="hidden" class="form-control" value="<?php echo $_GET['id'];?>" >
                                </div>
                                <div class="form-group">
                                    <input name="code"  class="form-control" value="<?php echo $code;?>" >
                                </div>
                                <div class="form-group">
                                    <input name="img" type="text" class="form-control" value="<?php echo $img;?>">
                                </div>
                                <div class="form-group">
                                    <textarea name="dis" class="form-control" rows="3" ><?php echo $dis;?></textarea>
                                </div>
                                <div class="form-group">
                                    <textarea name="dis_ad" class="form-control" rows="3" ><?php echo $dis_ad;?></textarea>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Изменить</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <!-- Left col -->
        </div>
        <!-- /.row -->
</div><!--/. container-fluid -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<!-- /.control-sidebar -->

<!-- Main Footer -->
<footer class="main-footer">
    <strong>Copyright © 2014-2020 <a href="https://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 3.1.0-rc
    </div>
</footer>
<div id="sidebar-overlay"></div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="plugins/raphael/raphael.min.js"></script>
<script src="plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard2.js"></script>

</body>
</html>

