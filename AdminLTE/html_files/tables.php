<div class="content-wrapper" style="min-height: 602px;">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0"><?php echo $_GET['category']?></h1>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- Left col -->

                        <!-- TABLE: LATEST ORDERS -->
                        <div class="card" style="width: 100%;">
                            <div class="card-header border-transparent">
                                <h3 class="card-title">Latest Orders</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-0">
                                <div class="col-md-2">
                                  <!--<form><input class="form-control" type="text" name="q" placeholder="Поиск" value="<?= $_GET['q'] ?>" id="search-text">
                                      <input type="hidden" name="category" value="<?= $_GET['category'] ?>">
                                      <input type="hidden" name="page" value="<?= $_GET['page'] ?>">
                                  </form>-->
                                    <input type="text" id = "search" name="referal" placeholder="Поиск" value="" class="form-control"  autocomplete="off"/>
                                    <ul id="display" class="search_result">
                                    </ul>

                                </div>
                                <div class="table-responsive">
                                    <table class="table m-0">
                                    <?php 
                                    AdminView::table($_GET['category']);
                                    ?>
                            <tbody id="search_out">

                                <?php
                                
                                if($_GET['category'] == "archive"){
                                    Admin::archive($_GET['q']);
                                }else if($_GET['category'] == "products"){
                                    Admin::prod($_GET['q']); 
                            }else if($_GET['category'] == "orders"){
                                    Admin::orders($_GET['q']);
                                }else if($_GET['category'] == "userstable"){
                                     Admin::users_table($_GET['q']);
                                } else Admin::laundry();
                                ?>

                            </tbody>
                                    </table>
                                    <?php
                                    $db = Db::getConnection();
                                    $maxpage = ceil($db->query("select count(*) from product")->fetch()[0]/10);
                                    $page = $_GET['page'];
                                    ?>
                                    Показаны товары <?= $offset+1 ?> - <?= $offset+11 ?>
                                   <ul class="">
                                       <?php if($page > 1): ?><li class="btn btn-primary"><a class="text-white" href="?q=<?= $_GET['q'] ?>&category=<?= $_GET['category']?>&page=1">First</li>
                                       <li class="btn btn-primary"><a class="text-white" href="?q=<?= $_GET['q'] ?>&category=<?= $_GET['category']?>&page=<?= $_GET['page']-1 ?>">Previous</a></li><?php endif; ?>
                                       <?php if($page != $maxpage): ?><li class="btn btn-primary"><a class="text-white" href="?q=<?= $_GET['q'] ?>&category=<?= $_GET['category']?>&page=<?= $_GET['page']+1 ?>">Next</a></li>
                                       <li class="btn btn-primary"><a class="text-white" href="?q=<?= $_GET['q'] ?>&category=<?= $_GET['category']?>&page=<?= $maxpage ?>">Last</a></li><?php endif; ?>
                                   </ul>
                                </div>
                                <!-- /.table-responsive -->
                               
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer clearfix">
                               <!-- <a href="javascript:void(0)" class="btn btn-sm btn-secondary float-right">View All Orders</a> -->
                            </div>
                            <!-- /.card-footer -->
                        </div>
                        <!-- /.card -->

                </div>
                <!-- /.row -->
            </div><!--/. container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <footer class="main-footer">
        <strong>Copyright © 2014-2020 <a href="#">AdminLTE.io</a>.</strong>
        All rights reserved.
    </footer>
    <div id="sidebar-overlay"></div></div>
<!-- ./wrapper -->