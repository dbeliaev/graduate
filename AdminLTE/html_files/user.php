<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 602px;">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Users</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-12">
                        <a href="index.php?category=userstable" class="btn btn-app">
                            <i class="fas fa-users"></i> Users
                        </a>
                    </div>
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <!-- Main row -->
                <div class="row">
                    <!-- Left col -->
                    <div class="col-md-6">
                        <!-- TABLE: LATEST ORDERS -->
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Изменение</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form action="../controls/change_delete_user.php" method="post" class="form-horizontal">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-2 col-form-label">Login</label>
                                        <div class="col-sm-10">
                                            <input name="login" type="text" class="form-control"  placeholder="Login">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
                                        <div class="col-sm-10">
                                            <input name="password" type="password" class="form-control"  placeholder="Password">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="offset-sm-2 col-sm-10">
                                            <div class="form-check">
                                                <input name="delete"  type="checkbox" class="form-check-input">
                                                <label class="form-check-label" for="exampleCheck2">Удалить</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-info">Изменить</button>
                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>

                    <!-- /.col -->
                    <div class="col-md-6">
                        <!-- TABLE: LATEST ORDERS -->
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Регистрация</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form action="../controls/save_user.php" method="post" class="form-horizontal">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-2 col-form-label">Имя</label>
                                        <div class="col-sm-10">
                                            <input name="name" type="text" class="form-control"  placeholder="Имя">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-2 col-form-label">Фамилия</label>
                                        <div class="col-sm-10">
                                            <input name="surname" type="text" class="form-control"  placeholder="Фамилия">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-2 col-form-label">Отчество</label>
                                        <div class="col-sm-10">
                                            <input name="midname" type="text" class="form-control"  placeholder="Отчество">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-2 col-form-label">Login</label>
                                        <div class="col-sm-10">
                                            <input name="login" type="text" class="form-control"  placeholder="Login">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
                                        <div class="col-sm-10">
                                            <input name="password" type="password" class="form-control"  placeholder="Password">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-info">Зарегистрироваться</button>
                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div><!--/. container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <strong>Copyright © 2014-2020 <a href="https://adminlte.io">AdminLTE.io</a>.</strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
            <b>Version</b> 3.1.0-rc
        </div>
    </footer>
    <div id="sidebar-overlay"></div></div>
<!-- ./wrapper -->
