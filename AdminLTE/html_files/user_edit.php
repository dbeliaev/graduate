<?php
include "../components/Autoload.php";
session_start();
Admin::Check();
$row = Admin::user_out($_GET['id']);
$role = $row['role_id'];


?>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 602px;">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">User</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Main row -->
            <div class="row">
                <div class="col-sm-6">
                    <div class="card card-primary">
                        <form action="../controls/ed_to_user.php" method="post">
                            <div class="card-header">
                                <h3 class="card-title">Изменение пользователя</h3>
                            </div>
                            <!-- /.card-header -->

                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleSelectRounded0">Главная кнопка!!!</label>
                                    <select name="role" class="custom-select rounded-0" >
                                        <?php
                                        $db = Db::getConnection();
                                        $sql =  $db->query('SELECT id, name FROM role');
                                        while ($result = $sql->fetch()) {
                                                if($result['id'] == $role) $select = 'selected'; else $select = '';
                                                echo "<option value='{$result['id']}' $select> {$result['name']}</option>";

                                        }
                                        ?>
                                    </select>
                                </div>
                                <h4>ЧТо-тО</h4>
                                <!-- text input -->
                                <div class="form-group">
                                    <input name="login" type="text" class="form-control" value="<?php echo $row['login'];?>" >
                                </div>
                                <div class="form-group">
                                    <input name="name" type="text" class="form-control" value="<?php echo $row['name'];?>">
                                </div>
                                <div class="form-group">
                                    <input name="midname"  class="form-control" value="<?php echo $row['midname'];?>" >
                                </div>
                                <div class="form-group">
                                    <input name="id" type="hidden" class="form-control" value="<?php echo $_GET['id'];?>" >
                                </div>
                                <div class="form-group">
                                    <input name="surname" type="text" class="form-control" value="<?php echo $row['surname'];?>" >
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Изменить</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <!-- Left col -->
        </div>
        <!-- /.row -->
</div><!--/. container-fluid -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<!-- /.control-sidebar -->

<!-- Main Footer -->
<footer class="main-footer">
    <strong>Copyright © 2014-2020 <a href="https://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 3.1.0-rc
    </div>
</footer>
<div id="sidebar-overlay"></div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="../plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="../plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="../plugins/raphael/raphael.min.js"></script>
<script src="../plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="../plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="../plugins/chart.js/Chart.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../dist/js/pages/dashboard2.js"></script>

</body>
</html>


