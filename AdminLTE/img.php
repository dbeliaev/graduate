<?php
include "../components/Autoload.php";
session_start();
Admin::Check();
include 'html_files/headhtml.php';
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 602px;">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Картинки</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Main row -->
            <div class="row">
                <div class="col-sm-6">
                    <div class="card card-primary">
                        <form action="../controls/img.php" method="post">
                            <div class="card-header">
                                <h3 class="card-title">Добавление картинки</h3>
                            </div>
                            <!-- /.card-header -->

                            <div class="card-body">
                                <!-- text input -->
                                <div class="form-group">
                                    <input name="img1" type="text" class="form-control" value="" >
                                </div>
                                <div class="form-group">
                                    <input name="id" type="hidden" class="form-control" value="<?php echo $_GET['id'];?>" >
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Добавить</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->

                    <div class="card card-primary">
                        <form action="../controls/img_del.php" method="post">
                            <div class="card-header">
                                <h3 class="card-title">Удаление картинки</h3>
                            </div>
                            <!-- /.card-header -->

                            <div class="card-body">
                                <!-- text input -->
                                <div class="form-group">
                                    <input name="img" type="text" class="form-control" value="" >
                                </div>
                                <div class="form-group">
                                    <input name="id" type="hidden" class="form-control" value="<?php echo $_GET['id'];?>" >
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="bnt btn-danger">Удалить</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->

                </div>

                <div class="col-sm-6">
                    <div class="col-sm-1"><i class="fa fa-arrow-left" onclick="if(now == 0){now = max-1; document.getElementById('gallery0').setAttribute('hidden', true); document.getElementById('gallery' + now).removeAttribute('hidden');} else{now--;document.getElementById('gallery' + (now+1)).setAttribute('hidden', true); document.getElementById('gallery' + now).removeAttribute('hidden');}"></i></div>
                    <div class="col-sm-1" style="position: absolute"><i class="fa fa-arrow-right" onclick="if(document.getElementById('gallery' + (now+1)) != null){now++;document.getElementById('gallery' + (now-1)).setAttribute('hidden', true); document.getElementById('gallery' + now).removeAttribute('hidden');}else{document.getElementById('gallery' + now).setAttribute('hidden', true); now=0; document.getElementById('gallery0').removeAttribute('hidden');}"></i></div>
                    <div class="img" style="padding-top: 30px; height: 1px!important;">
                        <?php
                        View::img($_GET['id']);
                        ?>
                        <script> var now = 0; </script>
                    </div>

                </div>
            </div>
            <!-- Left col -->
        </div>
        <!-- /.row -->
</div><!--/. container-fluid -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<!-- /.control-sidebar -->

<!-- Main Footer -->
<footer class="main-footer">
    <strong>Copyright © 2014-2020 <a href="https://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 3.1.0-rc
    </div>
</footer>
<div id="sidebar-overlay"></div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="plugins/raphael/raphael.min.js"></script>
<script src="plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard2.js"></script>

</body>
</html>

