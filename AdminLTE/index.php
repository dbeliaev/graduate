<?php
include "../components/Autoload.php";
session_start();

Admin::Check();

include 'html_files/headhtml.php';

if($_GET['category'] == "archive" || $_GET['category'] == "products" || $_GET['category'] == "orders" || $_GET['category'] == "laundry" || $_GET['category'] == "userstable"){
    include 'html_files/tables.php';  
} else if($_GET['category'] == "users"){
    include 'html_files/user.php';  
} else if($_GET['category'] == "edituser"){
    include 'html_files/user_edit.php';
}

include 'html_files/scripts.php'

?>
