<?php
include "../components/Autoload.php";
session_start();

Admin::Check();
?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>AdminLTE 3 | Dashboard </title>

        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
        <!-- overlayScrollbars -->
        <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
    </head>
    <body class="sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed sidebar-collapse" style="height: auto;">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="index.php" class="nav-link">Home</a>
                </li>
            </ul>

            <!-- SEARCH FORM -->
            <form class="form-inline ml-3">
                <div class="input-group input-group-sm">
                    <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                    <div class="input-group-append">
                        <button class="btn btn-navbar" type="submit">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>

        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->

<?php
include 'html_files/sidebar.php';
?>
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Продукты</h1>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Product</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="col-md-2">
                                    <form><input class="form-control" type="text" name="q" placeholder="Поиск" value="<?= $_GET['q'] ?>" id="search-text""></form>
                                    </div>
                                        <table id="product" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>Изображение</th>
                                            <th>Название</th>
                                            <th>Описание</th>
                                            <th>Доступно</th>
                                            <th>Зарезервировано</th>
                                            <th>Код</th>
                                            <th>Денйсвия</th>
                                        </tr>
                                        </thead>
                                <tbody>
                                <form action="edit_data.php" method="post">
                                <?php
                                $db = Db::getConnection();
                                $page = $_GET['page'] == "" ? 1 : $_GET['page'];
                                $offset = ($page - 1) * 10;
                                if($search_query != ''){
                                    $sql = $db->query("SHOW COLUMNS FROM product");
                                    while ($result = $sql->fetch()){
                                        if($search == ''){
                                            $search = 'WHERE '.$result['Field'].' LIKE \'%'.$search_query.'%\'';
                                        }else{
                                            $search .= ' OR '.$result['Field'].' LIKE \'%'.$search_query.'%\'';
                                        }
                                    }
                                }
                                
                                $sql = $db->query("SELECT * FROM product $search LIMIT 10 offset ".$offset);
                                while ($result = $sql->fetch()){
                                   echo "<tr>
                                         <td>{$result['id']}</td>
                                         <td><img src=".$result['img']." height='150px' width='100px'></td>
                                         <td>{$result['name']}</td>
                                         <td>{$result['description']}</td>
                                         <td>{$result['availability']}</td>
                                         <td>{$result['reserved']}</td>
                                         <td>{$result['code']}</td>
                                         <td>
                                         <a class='btn' onclick='deldata(".$result['id'].")'><i class='fa fa-minus-circle'></i></a>
                                         <a  href='edit_data.php' onclick='edit_data(".$result['id'].") class='btn'><i class= 'fa fa-pen'></i></a>
                                         </td>
                                         </tr>
                                         ";
                                }
                                ?>
                                </form>
                                </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>id</th>
                                            <th>Изображение</th>
                                            <th>Название</th>
                                            <th>Описание</th>
                                            <th>Доступно</th>
                                            <th>Зарезервировано</th>
                                            <th>Код</th>
                                            <th>Действия</th>
                                        </tr>
                                        </tfoot>
                                    </table>

                                    <?
                                    $maxpage = ceil($db->query("select count(*) from product")->fetch()[0]/10);
                                    ?>
                                    Показаны товары <?= $offset+1 ?> - <?= $offset+11 ?>
                                   <ul class="">
                                       <? if($page > 1): ?><li class="btn btn-primary"><a class="text-white" href="?page=1">First</li>
                                       <li class="btn btn-primary"><a class="text-white" href="?page=<?= $page-1 ?>">Previous</a></li><? endif; ?>
                                       <? if($page != $maxpage): ?><li class="btn btn-primary"><a class="text-white" href="?page=<?= $page+1 ?>">Next</a></li>
                                       <li class="btn btn-primary"><a class="text-white" href="?page=<?= $maxpage ?>">Last</a></li><? endif; ?>
                                   </ul>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
                <b>Version</b> 3.1.0-rc
            </div>
            <strong>Copyright &copy; 2014-2020 </strong> AdminLTE.io  All rights reserved.
        </footer>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="../template/admin/js/datatables.js"></script>
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="plugins/jszip/jszip.min.js"></script>
    <script src="plugins/pdfmake/pdfmake.min.js"></script>
    <script src="plugins/pdfmake/vfs_fonts.js"></script>
    <script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- Page specific script -->
    <!--<script>
        $(function () {
            $("#example").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": true,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "responsive": true,
            });
        });
    </script>-->

    </body>
</html>


