<?php

/**
 * Автозагрузка классов
 */
spl_autoload_register(function (string $className) {

    //директории, в которых будет идти поиск классов
    $arrayPath = array(
        '/../models/',
        '/../components/',
        '/../controls/',
        '/../PhpWord/library/'
    );

    foreach ($arrayPath as $path) {
        $path = __DIR__ . $path . $className . '.php';
        if (is_file($path))
            include_once $path;
    }
} );

$cssver = 1;