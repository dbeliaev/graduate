<?php


class Admin
{
    static function Check()
    {
        if ($_SESSION['role_id'] == 2) {
            return 0;
        } else header("Location: /index.php");

    }


    static function del_from_db($id)
    {
        $db = Db::getConnection();
        $db->query("DELETE FROM product WHERE id = $id ");
        $db->query("DELETE FROM img WHERE product_id = $id ");
        return 0;
    }

    static function del_from_order($id)
    {
        $db = Db::getConnection();
        $sql = $db->query('UPDATE product_order SET status = 4 WHERE id = ' . $id . ' ');
        return 0;
    }

    static function edit_data($id)
    {
        $db = Db::getConnection();
        $sql = $db->query('SELECT * FROM product WHERE id = ' . $id . '');
        $myrow = $sql->fetch();
        return $myrow;
    }


    static function cart_admin($id)
    {
        $db = Db::getConnection();
        $sql = 'SELECT products_id FROM product_order WHERE id = :id';
        $res = $db->prepare($sql);
        $res->bindParam(':id', $id, PDO::PARAM_INT);
        $res->execute();
        $cart = $res->fetch();
        $cart = unserialize(base64_decode($cart['products_id']));
        return $cart;
    }

    static function admin_cart_out($cart, $or_id)
    {
        $db = Db::getConnection();
        foreach ($cart as $key => $item) {
            $sql = $db->query("SELECT id,name,img,code,category_id,description_ad FROM product WHERE id={$item['id']}");
            while ($result = $sql->fetch()) {
                if ($result['category_id'] == 3) {
                    $def = '<a href="#" onclick="RetFromOrderLaundry(' . $result['id'] . ', ' . $or_id . ')"><i class="fa fa-undo-alt"></i></a>';
                } else $def = '<a href="#" onclick="RetFromOrder(' . $result['id'] . ', ' . $or_id . ')"><i class="fa fa-undo-alt"></i></a>';
                echo '
				    	<tr>
							<td >
								<img src="' . $result['img'] . '" width="100px" height="150px" alt="">
							</td>
							<td>
								<p>' . $result["name"] . '</p>
							</td>
							<td>
                            <p>' . $cart[$key]['count'] . '</p>
							</td>
							<td>
							<p> ' . $result["description_ad"] . '</p>
							</td>
                            <td>
							<p>Web ID: ' . $result["code"] . '</p>
							</td>
							<td>
								' . $def . '
							</td>
						</tr>
						    ';
            }
            continue;
            return;
        }
    }

    static function cart_admin_l($id){
        $db = Db::getConnection();
        $sql = 'SELECT products_id FROM laundry WHERE id = :id';
        $res = $db->prepare($sql);
        $res->bindParam(':id', $id, PDO::PARAM_INT);
        $res->execute();
        $cart = $res->fetch();
        $cart = unserialize(base64_decode($cart['products_id']));

        Admin::admin_cart_out_l($cart);
        return;
    }

    static function admin_cart_out_l($cart){
        $db = Db::getConnection();
        foreach ($cart as $key => $item) {
            $sql = $db->query("SELECT id,name,img,code,category_id,description_ad FROM product WHERE id={$item['id']}");
            while ($result = $sql->fetch()) {
                echo '
				    	<tr>
							<td >
								<img src="' . $result['img'] . '" width="100px" height="150px" alt="">
							</td>
							<td>
								<p>' . $result["name"] . '</p>
							</td>
							<td>
                            <p>' . $cart[$key]['laundry'] . '</p>
							</td>
							<td>
							<p> ' . $result["description_ad"] . '</p>
							</td>
                            <td>
							<p>Web ID: ' . $result["code"] . '</p>
							</td>
						</tr>
						    ';
            }
            continue;
            return;
        }
    }


    static function order_clance_prepare($id)
    {
        $db = Db::getConnection();
        $sql = 'SELECT products_id FROM product_order WHERE id = :id';
        $res = $db->prepare($sql);
        $res->bindParam(':id', $id, PDO::PARAM_INT);
        $res->execute();
        $cart = $res->fetch();
        $cart = unserialize(base64_decode($cart['products_id']));
        Admin::order_clance($cart);
        $sql2 = $db->query('UPDATE product_order SET status = 4 WHERE id = ' . $id . ' ');
        return;
    }

    static function order_clance($cart)
    {
        $db = Db::getConnection();
        foreach ($cart as $key => $item) {
            $id = $item['id'];
            $count = $cart[$key]['count'];
            $sql = $db->query('SELECT availability,reserved FROM product WHERE id = ' . $id . '');
            $myrow = $sql->fetch();
            $final = $myrow['reserved'] - $count;
            $av = $myrow['availability'] + $count;
            $sql2 = $db->query('UPDATE product SET reserved = ' . $final . ', availability = ' . $av . ' WHERE id = ' . $id . '');
            continue;
        }
        return;
    }


    static function order_sucsess($id)
    {
        $db = Db::getConnection();
        $sql = $db->query('UPDATE product_order SET status = 2 WHERE id = ' . $id . ' ');
    }

    static function order_satus_prepare($id)
    {
        $db = Db::getConnection();
        $sql = 'SELECT products_id FROM product_order WHERE id = :id';
        $res = $db->prepare($sql);
        $res->bindParam(':id', $id, PDO::PARAM_INT);
        $res->execute();
        $cart = $res->fetch();
        $cart = unserialize(base64_decode($cart['products_id']));
        Admin::orderStatus($cart);
        $sql2 = $db->query('UPDATE product_order SET status = 5 WHERE id = ' . $id . ' ');
        return;
    }


    static function orderStatus($cart)
    {
        $db = Db::getConnection();
        foreach ($cart as $key => $item) {
            $id = $item['id'];
            $count = $cart[$key]['count'];
            $sql = $db->query('SELECT reserved FROM product WHERE id = ' . $id . '');
            $myrow = $sql->fetch();
            $final = $myrow['reserved'] - $count;
            $sql2 = $db->query('UPDATE product SET reserved = ' . $final . ' WHERE id = ' . $id . '');
            continue;
        }
        return;
    }

    static function returnOrderPreparee($id, $count, $cart){
        foreach ($cart as $key => $item) {
            if ($item['id'] == $id) {
                $co = $cart[$key]['count'];
                if ($co >= $count) Admin::returnOrder($id, $count);
                else exit();
            }
        }
    }
    static function returnOrder($id, $count)
    {
        $db = Db::getConnection();
        $result = $db->query('SELECT availability FROM product WHERE id= ' . $id . '');
        $myrow = $result->fetch();
        $availability = $myrow['availability'];
        $final = $availability + $count;
        $result2 = $db->query("UPDATE product SET availability = '$final' WHERE id= '$id'");
        return 0;
    }

    //Возврат со стиркой
    static function returnOrderPrepare($cart, $id, $count, $laundry){
        $final = $laundry + $count;
        foreach ($cart as $key => $item) {
            if ($item['id'] == $id) {
                $co = $cart[$key]['count'];
                if ($co >= $final) Admin::returnOrderLaundry($id, $count, $laundry);
                else exit();
            }
        }
}
    static function returnOrderLaundry($id, $count, $laundry){
        $db = Db::getConnection();
        $result2 = $db->query("UPDATE product SET availability = availability + $count, laundry = laundry + $laundry WHERE id = $id");
        $result = $db->query("SELECT * FROM laundry");
        if ($result->rowCount() == 0){
            $cart = array(array('id' => $id, 'laundry' => $laundry));
            $cart = base64_encode(serialize($cart));
            $res = $db->query("INSERT INTO laundry (products_id) VALUES('$cart')");
        }else{
            while ($myrow = $result->fetch()) {
                if ($myrow['status'] == 1) {
                    $cart = unserialize(base64_decode($myrow['products_id']));
                    foreach ($cart as $key => $item) {
                        if ($item['id'] == $id) {
                            $cart[$key]['laundry'] += $laundry;
                            $cart = base64_encode(serialize($cart));
                            $result1 = $db->query("UPDATE laundry SET products_id = '$cart' WHERE id = {$myrow['id']}");
                            return;
                        }
                    }
                    $cart [] = array('id' => $id, 'laundry' => $laundry);
                    $cart = base64_encode(serialize($cart));
                    $result3 = $db->query("UPDATE laundry SET products_id = '$cart' WHERE id = {$myrow['id']}");
                    $flag = true;
                }
            }
            if(!$flag) {
                $cart = array(array('id' => $id, 'laundry' => $laundry));
                $cart = base64_encode(serialize($cart));
                $res = $db->query("INSERT INTO laundry (products_id) VALUES('$cart')");
            }
        }
        return 0;
    }

    //Корзина стирки вывод
    static function admin_laundry_out($cart)
    {
        $db = Db::getConnection();
        foreach ($cart as $key => $item) {
            //$sql = $db->query('SELECT `name`, `img`, `code` FROM `product`  WHERE id == $item["id"]');
            $sql = $db->query("SELECT id,name,img,code,category_id,description_ad FROM product WHERE id={$item['id']}");
            while ($result = $sql->fetch()) {
                if($result['category_id'] == 3){
                    $def = '<a href="#" onclick="RetFromOrderLaundry('.$result['id'].')('.$result['id'].')"><i class="fa fa-undo-alt"></i></a>';
                }else $def = '<a href="#" onclick="RetFromOrder('.$result['id'].')"><i class="fa fa-undo-alt"></i></a>';
                echo '
				    	<tr>
							<td >
								<img src="' . $result['img'] . '" width="100px" height="150px" alt="">
							</td>
							<td>
								<p>' . $result["name"] . '</p>
							</td>
							<td>
                            <p>' . $cart[$key]['count'] . '</p>
							</td>
							<td>
							<p> ' . $result["description_ad"] . '</p>
							</td>
                            <td>
							<p>Web ID: ' . $result["code"] . '</p>
							</td>
							<td>
								'.$def.'
							</td>
						</tr>
						    ';
            }
            continue;
            return;
        }
    }
     //Заказы вывод
    static function orders($search_query){
        $category ='orders';
        $db = Db::getConnection();
        $page = $_GET['page'] == "" ? 1 : $_GET['page'];
        $offset = ($page - 1) * 25;
        if($search_query != ''){
            $sql = $db->query("SHOW COLUMNS FROM product_order");
            while ($result = $sql->fetch()){
                if($search == ''){
                    $search = 'WHERE '.$result['Field'].' LIKE \'%'.$search_query.'%\'';
                }else{
                    $search .= ' OR '.$result['Field'].' LIKE \'%'.$search_query.'%\'';
                }
            }
        }
        $sql = $db->query("SELECT * FROM product_order WHERE status != 4 $search LIMIT 25 offset ".$offset);
        while ($result = $sql->fetch()){
            $status = $result['status'];
            $memo = $result['status_memo'];
            $id = $result['id'];
            if($memo == "0") $memo1 = "<span style= 'background-color : red' >$id-0</span>";
            if($memo == "1") $memo1 = "<span style= 'background-color : green' >$id-1</span>";

            if($status == "1") $status1 = "Заказ принят";

            if($status == "2") $status1 = "Заказ готов к выдаче";
            if($status == "5") $status1 = "Заказ выдан";
            if($status == "3") $status1 = "Заказ отменён";
                $id = $result['id'];
                if($status == 2) $success = "<a onclick='order_status($id)' class='btn btn-success'><i class='fa fa-check'></i></a>";
                if($status == 1) $success = "<a onclick='order_suc($id)'  class='btn btn-primary'><i class='fa fa-check'></i> </a>"; else $completed = '';
                if($status == 3)
                {
                    $ret = "<a onclick='order_stat_clance($id)'  class='btn btn-danger'><i class='fa fa-check'></i></a> ";
                    $del = '';
                }
                if($status != 3){
                    $del = "<a onclick='order_stat_clance($id)' class='btn btn-primary'><i class='fa fa-trash'></i></a>";
                    $ret = '';
                }
                if($status == 5){
                    $del = "<a onclick='delete_from_order($id)' class='btn btn-primary'><i class='fa fa-trash'></i></a>";
                    $success = '';
                }
                echo "<tr>
             <td>{$memo1}</td>
             <td>{$result['user_name']}</td>
             <td>{$result['date']}</td>
             <td>{$result['eventdate']}</td>
             <td>{$result['user_comment']}</td>
             <td>{$result['memo_comment']}</td>
             <td>{$status1}</td>
             <td>
             <a  href='cart.php?id= $id' class='btn btn-primary'><i class='fa fa-shopping-cart'></i></a>
             $ret  
             $success
             $del                            
             </td>
             </tr>
             ";

        }
    }
     //Архив вывод
    static function archive($search_query){
        $db = Db::getConnection();
        $page = $_GET['page'] == "" ? 1 : $_GET['page'];
        $offset = ($page - 1) * 10;
        if($search_query != ''){
            $sql = $db->query("SHOW COLUMNS FROM product_order");
            while ($result = $sql->fetch()){
                if($search == ''){
                    $search = 'WHERE '.$result['Field'].' LIKE \'%'.$search_query.'%\'';
                }else{
                    $search .= ' OR '.$result['Field'].' LIKE \'%'.$search_query.'%\'';
                }
            }
        }
        $sql = $db->query("SELECT * FROM product_order WHERE status = 4 $search LIMIT 10 offset " .$offset);
        while ($result = $sql->fetch()){
                $id = $result['id'];
                $status = "Архив";
                echo "<tr>
             <td>{$result['id']}</td>
             <td>{$result['user_name']}</td>
             <td>{$result['date']}</td>
             <td>{$result['user_comment']}</td>
             <td>{$status}</td>
             <td>
             <a  href='cart.php?id= $id' class='btn btn-primary'><i class='fa fa-shopping-cart'></i></a>
             <a href='#' onclick='delarc($id)' class='btn btn-primary'><i class='fa fa-trash'></i></a>                                     
             </td>
             </tr>
             ";
            }
            
        }

    static function del_from_archive($id){
        $db = Db::getConnection();
        $sql = 'DELETE FROM product_order WHERE id =:id';
        $res = $db->prepare($sql);
        $res->bindParam(':id', $id, PDO::PARAM_INT);
        $res->execute();
        
        return;
    }

    static function prod($search_query){
        $categor ='products';
        $db = Db::getConnection();
        $page = $_GET['page'] == "" ? 1 : $_GET['page'];
        $offset = ($page - 1) * 10;
        if($search_query != ''){
            $sql = $db->query("SHOW COLUMNS FROM product");
            while ($result = $sql->fetch()){
                if($search == ''){
                    $search = 'WHERE '.$result['Field'].' LIKE \'%'.$search_query.'%\'';
                }else{
                    $search .= ' OR '.$result['Field'].' LIKE \'%'.$search_query.'%\'';
                }
            }
        }
        
        $sql = $db->query("SELECT * FROM product $search LIMIT 10 offset ".$offset);
        while ($result = $sql->fetch()){
           echo "<tr id='table'>
                 <td>{$result['id']}</td>
                 <td><img src=".$result['img']." height='150px' width='100px'></td>
                 <td>{$result['name']}</td>
                 <td>{$result['description']}</td>
                 <td>{$result['description_ad']}</td>
                 <td>{$result['availability']}</td>
                 <td>{$result['reserved']}</td>
                 <td>{$result['code']}</td>
                 <td>{$result['date']}</td>
                 <td>
                 <a class='btn' onclick='deldata(".$result['id'].")'><i class='fa fa-minus-circle'></i></a>
                 <a  href='edit_data.php?id={$result['id']}' class='btn'><i class= 'fa fa-pen'></i></a>
                 <a  class='btn' href='img.php?id={$result['id']}'><i class='fa fa-file-image'></i></a>
                 </td>
                 </tr>
                 ";
        }
        return $offset;
    }

    static function users($search_query){
        $category ='usertable';
        $db = Db::getConnection();
        $page = $_GET['page'] == "" ? 1 : $_GET['page'];
        $offset = ($page - 1) * 10;
        if($search_query != ''){
            $sql = $db->query("SHOW COLUMNS FROM product");
            while ($result = $sql->fetch()){
                if($search == ''){
                    $search = 'WHERE '.$result['Field'].' LIKE \'%'.$search_query.'%\'';
                }else{
                    $search .= ' OR '.$result['Field'].' LIKE \'%'.$search_query.'%\'';
                }
            }
        }

        $sql = $db->query("SELECT * FROM users $search LIMIT 10 offset ".$offset);
        while ($result = $sql->fetch()){
            echo "<tr>
                 <td>{$result['id']}</td>
                 <td><img src=".$result['img']." height='150px' width='100px'></td>
                 <td>{$result['login']}</td>
                 <td>{$result['name']}</td>
                 <td>{$result['midname']}</td>
                 <td>{$result['surname']}</td>
                 <td>{$result['role_id']}</td>
                 <td>
                 <a  href='#' class='btn'><i class= 'fa fa-pen'></i></a>
                 </td>
                 </tr>
                 ";
        }
        return $offset;
    }


    //Таблица Стирки
    public static function laundry($search_query){
        $category ='laundry';
        $db = Db::getConnection();
        $page = $_GET['page'] == "" ? 1 : $_GET['page'];
        $offset = ($page - 1) * 25;
        if($search_query != ''){
            $sql = $db->query("SHOW COLUMNS FROM laundry");
            while ($result = $sql->fetch()){
                if($search == ''){
                    $search = 'WHERE '.$result['Field'].' LIKE \'%'.$search_query.'%\'';
                }else{
                    $search .= ' OR '.$result['Field'].' LIKE \'%'.$search_query.'%\'';
                }
            }
        }
        $sql = $db->query("SELECT * FROM laundry $search LIMIT 25 offset ".$offset);
        while ($result = $sql->fetch()){
            $status = $result['status'];
            if($status == "1") $status1 = "Сбор";
            if($status == "2") $status1 = "Отправлено";
                $id = $result['id'];
                echo "<tr>
             <td>{$result['id']}</td>
             <td>{$result['date']}</td>
             <td>{$status1}</td>
             <td>
             <a href='#' onclick='laundry_status($id)'  class='btn btn-primary'><i class='fa fa-check'></i> </a>
             <a  href='cart_laundry.php?id= $id' class='btn btn-primary'><i class='fa fa-shopping-cart'></i></a>                        
             </td>
             </tr>
             ";

        }
    }
    //Статус стирки
    public static function laundry_stat($id){
        $db = Db::getConnection();
        $result = $db->query("SELECT * FROM laundry WHERE id = $id ");
        while ($myrow = $result->fetch()) {
            if ($myrow['status'] == 2 ){
                $cart = unserialize(base64_decode($myrow['products_id']));
                foreach ($cart as $key => $item) {
                   $lau = $cart[$key]['laundry'];
                    $result2 = $db->query("UPDATE product SET availability = availability + $lau, laundry = laundry - $lau WHERE id={$item['id']}");
                    $result3 = $db->query("DELETE FROM laundry WHERE id = $id");
                }
                } else if ($myrow['status'] == 1) {
                $result1 = $db->query("UPDATE laundry SET status = 2 WHERE id = $id");
            }
        }

    }

    //Пользователи таблица

   public static function users_table($search_query){
        $category ='userstable';
        $db = Db::getConnection();
        $page = $_GET['page'] == "" ? 1 : $_GET['page'];
        $offset = ($page - 1) * 25;
        if($search_query != ''){
            $sql = $db->query("SHOW COLUMNS FROM users");
            while ($result = $sql->fetch()){
                if($search == ''){
                    $search = 'WHERE '.$result['Field'].' LIKE \'%'.$search_query.'%\'';
                }else{
                    $search .= ' OR '.$result['Field'].' LIKE \'%'.$search_query.'%\'';
                }
            }
        }
        $sql = $db->query("SELECT * FROM users  $search LIMIT 25 offset ".$offset);
        while ($result = $sql->fetch()) {
            $role_id = $result['role_id'];
            $sql2 = $db->query('SELECT name FROM role WHERE id = ' . $role_id . ' ');
            while ($role = $sql2->fetch()) {
                $id = $result['id'];
                echo "<tr>
             <td>{$id}</td>
             <td>{$result['login']}</td>
             <td>{$result['name']}</td>
             <td>{$result['surname']}</td>
             <td>{$result['midname']}</td>
             <td>{$role['name']}</td>
             <td><a href='edit_user.php?id=$id'>Изменить</a> </td>
             </tr>
             ";


            }
        }
    }

   public  static function psswd_check($id,$password){
        $db = Db::getConnection();
        $sql = $db->query('SELECT psswd FROM users WHERE id = '.$id.' ');
        while ($result = $sql->fetch()){
            if($password === $result['psswd']){
                $sql2 = $db->query('UPDATE  users SET psswd = null WHERE id = '.$id.' ');
                return true;
            } else {
                return false;
            }
        }

    }

   public static function user_out($id){
        $db = Db::getConnection();
        $sql = 'SELECT * FROM users WHERE id = :id ';
        $res  = $db->prepare($sql);
        $res->bindParam(':id', $id);
        $res->execute();
        return $res->fetch();
    }

   public static function edit_user($id){

   }
}