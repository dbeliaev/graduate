<?php
session_start();
class Cart
{
    static function addToCard($id)
    {
        $count = 1;
        if ($_SESSION['cart'] == null) $_SESSION['cart'] = array(array('id' => $id, 'count' => $count));
        else {
            foreach ($_SESSION['cart'] as $key => $item) {
                if ($item['id'] == $id) {
                    $_SESSION['cart'][$key]['count']++;
                    return;
                }
            }
            $_SESSION['cart'] [] = array('id' => $id, 'count' => $count);
        }

    }
    static function count_cart($id, $count){
        foreach ($_SESSION['cart'] as $key => $item) {
            if ($item['id'] == $id) {
                $_SESSION['cart'][$key]['count'] = $count;
                return;
            }
        }
    }
    static function minus_cart($id){
        foreach ($_SESSION['cart'] as $key => $item) {
            if ($item['id'] == $id) {
                $_SESSION['cart'][$key]['count']--;
                return;
            }
        }
    }
   public static function removeFromCard($id){
       foreach ($_SESSION['cart'] as $key => $item) {
           if ($item['id'] == $id) {
               unset($_SESSION['cart'][$key]);
               return;
           }
       }
    }

    static function out()
    {
        $db = Db::getConnection();
        foreach ($_SESSION['cart'] as $key => $item) {
            if ( $_SESSION['cart'][$key]['count'] == 0) {
                unset($_SESSION['cart'][$key]);
            }
            //$sql = $db->query('SELECT `name`, `img`, `code` FROM `product`  WHERE id == $item["id"]');
            $sql = $db->query("SELECT id, name,img,code,availability FROM product WHERE id={$item['id']}");
            while ($result = $sql->fetch()) {
                    echo ' <tbody>
                  <form action=" ">
						<tr>
							<td class="cart_product">
								<a href=""><img src="'.$result['img'].'" width="100px" height="150px" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a>'.$result["name"].'</a></h4>
								<p>Web ID: '.$result["code"].'</p>
							</td>

							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class= "cart_quantity_up" id='.$result["id"] .' onClick="addToCart('. $result["id"] .')" href="cart.php" > + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="'. $_SESSION['cart'][$key]['count'].'" oninput="count_cart('.$result["id"].', this.value)" autocomplete="off" size="3">
									<a class="cart_quantity_down" id='.$result["id"] .' onClick="minusCart('. $result["id"] .')" href="cart.php"> - </a>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">'.$result["availability"].'</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" onclick="delFromCart('.$result['id'].')" href="#"><i class="fa fa-times"></i></a>
							</td>
						</tr>
						</form>
						</tbody>
						';


            }
          continue;
          return;
        }

    }


    static function delet_order($id){
        $db = Db::getConnection();
        $sql = 'UPDATE product_order SET status = 3 WHERE id = :id';
        $res = $db->prepare($sql);
        $res->bindParam(':id', $id, PDO::PARAM_INT);
        $res->execute();

        return 0;
    }

}
