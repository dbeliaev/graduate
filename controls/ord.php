<?php
session_start();
//require_once "../public/cart.php";
require __DIR__ . "/../components/Autoload.php";
if(empty($_SESSION['id'])){
    header("Location:../index.php");
    exit;
}
if(empty($_SESSION['cart'])){
    header("Location:../public/cart.php?error=".urlencode('Ваша корзина пуста!'));
    exit;
}
if(isset($_POST['comment'])){ $com = $_POST['comment'];}
if(empty($_POST['comment'])){
    header("Location:../public/cart.php?error=".urlencode('Заполните комментарий и не забудьте указать дату!'));
    exit;
    echo 'Заполните комментарий и не забудьте указать дату!';
}
if(isset($_POST['event_date'])){ $eventdate = $_POST['event_date'];}
if(empty($_POST['event_date'])){
    header("Location:../public/cart.php?error=".urlencode('Укажитеw дату!'));
    exit;

}
$com = stripslashes($com);
$com = htmlspecialchars($com);
$cart = base64_encode(serialize($_SESSION['cart']));
$id = $_SESSION['id'];
$name = $_SESSION['login'];
$today = date("Y-m-d");
// Подключение к бд
$db = Db::getConnection();
//Проверка даты
if($eventdate < $today){
    header("Location:../public/cart.php?error=".urlencode('Дата мероприятия не может быть в прошлом!'));
    exit();
}

foreach ($_SESSION['cart'] as $key => $item){
	$res = $item['id'];
	$count = $_SESSION['cart'][$key]['count'];
	$result2 = $db->query("SELECT reserved, availability FROM product WHERE id= '$res'");
	$myrow =$result2->fetch();
	$reserved = $myrow['reserved'];
	$availability = $myrow['availability'];
	if($availability < $count){
		header("Location:../public/cart.php?error=".urlencode('Введите количество товара, не превышающее количество доступного'));
		exit;
	}
}
foreach ($_SESSION['cart'] as $key => $item){
	$res = $item['id'];
	$count = $_SESSION['cart'][$key]['count'];
	$result2 = $db->query("SELECT reserved, availability FROM product WHERE id= '$res'");
	$myrow =$result2->fetch();
	$reserved = $myrow['reserved'];
	$availability = $myrow['availability'];
	$av = $availability - $count;
	$final = $reserved + $count;
	$result3 =  $db->query("UPDATE product SET availability = '$av', reserved =' $final' WHERE id= '$res'");
}
$result = $db->query("INSERT INTO product_order (user_name, user_comment, user_id, eventdate, products_id) VALUES('$name','$com','$id','$eventdate','$cart')");

if ($result==TRUE)
{
    $or = "";
    foreach ($_SESSION['cart'] as $key => $item){
        $res = $item['id'];
        $count = $_SESSION['cart'][$key]['count'];
        $result4 = $db->query("SELECT name FROM product WHERE id= '$res'");
        $myrow =$result4->fetch();
        $name = $myrow['name'];
        $or .= " $name : $count , \n";
    }
    $uid = '241634016,223191375';
    $msg = urlencode("Новый заказ: \nПользователь: {$_SESSION['login']} \n$com \n $or");
    $vktoken = '2968def62dfee102ebc8e398c3bcfd76740e89907a034418830f02eb8a4085a212697c81cfe8028b11c3a';
    file_get_contents("https://api.vk.com/method/messages.send?v=5.130&peer_ids=".$uid."&random_id=".rand(100, 2000000000)."&message=".$msg."&access_token=".$vktoken);
   unset($_SESSION['cart']);
    header("Location:/public/orders.php");

    //header("Location:/public/orders.php");
}
else {
    header("Location:../public/cart.php?error=".urlencode('Произошла ошибка! Попробуйте снова!'));
}