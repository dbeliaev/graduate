<?php
session_start();
require __DIR__ . "/../components/Autoload.php";
$db = Db::getConnection();
$id = $_SESSION['id'];
$token = null;
$sql = 'UPDATE users SET token =? WHERE id =?';
$db->prepare($sql)->execute([$token,$id]);
session_destroy();
header("Location:../index.php");
foreach($_COOKIE as $key => $value) setcookie($key, '', time() - 1, '/');
?>