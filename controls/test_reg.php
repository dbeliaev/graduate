<?php
    session_start();
   require __DIR__ . "/../components/Autoload.php";
   //  вся процедура работает на сессиях. Именно в ней хранятся данные  пользователя, пока он находится на сайте. Очень важно запустить их в  самом начале странички!!!
if (isset($_POST['login'])) { $login = $_POST['login']; if ($login == '') { unset($login);} } //заносим введенный пользователем логин в переменную $login, если он пустой, то уничтожаем переменную
    if (isset($_POST['password'])) { $password=$_POST['password']; if ($password =='') { unset($password);} }
    //заносим введенный пользователем пароль в переменную $password, если он пустой, то уничтожаем переменную
if (empty($login) or empty($password)) //если пользователь не ввел логин или пароль, то выдаем ошибку и останавливаем скрипт
    {
    exit ("Вы ввели не всю информацию, вернитесь назад и заполните все поля!");
    }
//если логин и пароль введены,то обрабатываем их, чтобы теги и скрипты не работали, мало ли что люди могут ввести
//   $login = Verification::attack($login);
//   $password = Verification::attack($password);
    $login = stripslashes($login);
    $login = htmlspecialchars($login);
    $password = stripslashes($password);
    $password = htmlspecialchars($password);
//удаляем лишние пробелы
    $login = trim($login);
    $password = md5(trim($password));
//извлекаем из базы все данные о пользователе с введенным логином
    $myrow = User::checkUserData($login);
    if (empty($myrow['password']))
    {
    //если пользователя с введенным логином не существует
    exit ("Извините, введённый вами login или пароль неверный.!");
    }
    else {
    //если существует, то сверяем пароли
    if ($myrow['password']==$password && ($myrow['role_id']== "1" || $myrow['role_id']== "3")) {
    //если пароли совпадают, то запускаем пользователю сессию! Можете его поздравить, он вошел!
    $_SESSION['login']=$myrow['login'];
    $_SESSION['role_id']=$myrow['role_id'];
    $_SESSION['id']=$myrow['id'];
    $_SESSION['token'] = bin2hex(random_bytes(32));
    User::token($myrow['id']);
    $_SESSION['cart']= array();//эти данные очень часто используются, вот их и будет "носить с собой" вошедший пользователь
    header("Location:../public/first.php");
    }
    else if ($myrow['password']==$password && $myrow['role_id']== "2"){
        $_SESSION['login']=$myrow['login'];
        $_SESSION['role_id']=$myrow['role_id'];
        $_SESSION['id']=$myrow['id'];//эти данные очень часто используются, вот их и будет "носить с собой" вошедший пользователь
        header("Location:../AdminLTE/index.php?category=orders");
    }
 else {
    //если пароли не сошлись

    exit ("Извините, введённый вами login или пароль неверный!!!!.");
    }
    }
    ?>