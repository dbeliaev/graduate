<?php
//  вся процедура работает на сессиях. Именно в ней хранятся данные  пользователя, пока он находится на сайте. Очень важно запустить их в  самом начале странички!!!
    session_start();
require __DIR__ . "/components/Autoload.php";
    ?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Login | E-Shopper</title>
    <link href="template/css/bootstrap.min.css" rel="stylesheet">
    <link href="template/css/font-awesome.min.css" rel="stylesheet">
    <link href="template/css/prettyPhoto.css" rel="stylesheet">
    <link href="template/css/price-range.css" rel="stylesheet">
    <link href="template/css/animate.css" rel="stylesheet">
	<link href="template/css/main.css" rel="stylesheet">
	<link href="template/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="template/js/html5shiv.js"></script>
    <script src="template/js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="template/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="template/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="template/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="template/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="template/images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Вход</h2>
                        <form action="controls/test_reg.php" method="post">
							<input name="login" type="text" placeholder="Логин" />
							<input name="password" type="password" placeholder="Пароль" />
							<span>
								<input type="checkbox" class="checkbox"> 
								Запомнить меня
							</span>
							<button type="submit" class="btn btn-default">Вход</button>
						</form>
					</div><!--/login form-->
                </div>
            </div>
		</div>
    </section>


<script src="template/js/jquery.js"></script>
	<script src="template/js/price-range.js"></script>
    <script src="template/js/jquery.scrollUp.min.js"></script>
	<script src="template/js/bootstrap.min.js"></script>
    <script src="template/js/jquery.prettyPhoto.js"></script>
    <script src="template/js/main.js"></script>
</body>
</html>
