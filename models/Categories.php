<?php
require __DIR__ . "/../components/Autoload.php";

class Categories
{
    public static function header_cat($id)
    {
        $db = Db::getConnection();

        $sql = "SELECT `name`, `href_us`, `role_id` FROM `header_cat`";

        $catList = $db->query($sql);
        while ($res = $catList->fetch()) {
            if ($res['role_id'] == $id || $res['role_id'] == '0') {
                echo "<li><a href='{$res['href_us']}'> {$res['name']}</a> </li>";
            }
        }
        return true;
    }

    public static function category($id)
    {
        $db = Db::getConnection();
        $sql = 'SELECT name, href_us FROM category WHERE header_cat_id = :id';
        $res = $db->prepare($sql);
        $res->bindParam(':id', $id, PDO::PARAM_INT);
        $res->execute();
        while ($result = $res->fetch()) {
            echo '
                <li><a href='.$result['href_us'].'> '.$result['name'].'</a> </li>
                ';
        }
        return true;
    }

    public static function subcategory($id)
    {
        $db = Db::getConnection();
        $sql = 'SELECT name, href_us FROM subcategory WHERE category_id = :id';
        $res = $db->prepare($sql);
        $res->bindParam(':id', $id, PDO::PARAM_INT);
        $res->execute();
        while ($result = $res->fetch()) {
            echo "<li><a href='{$result['href_us']}'> {$result['name']}</a> </li>";
        }
        return true;
    }

    public static function final_cat($id)
    {
        $db = Db::getConnection();
        $sql = 'SELECT name, href_us FROM final_cat WHERE subcategory_id = :id';
        $res = $db->prepare($sql);
        $res->bindParam(':id', $id, PDO::PARAM_INT);
        $res->execute();
        while ($result = $res->fetch()) {
            echo "<li><a href='{$result['href_us']}'> {$result['name']}</a> </li>";
        }
        return true;


    }
}