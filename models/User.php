<?php
session_start();
/**
 * Класс User - модель для работы с пользователями
 */
class User
{
    /**
     * Возвращает идентификатор пользователя, если он авторизирован.<br/>
     * Иначе перенаправляет на страницу входа
     * @return string <p>Идентификатор пользователя</p>
     */
    public static function checkLogged()
    {
        // Если сессия есть, вернем идентификатор пользователя
        if (isset($_SESSION['login'])) {
            return 0;
        } else{
            header("Location: ../index.php");
        }

    }
    public static function checkVip()
    {
        // Если сессия есть, вернем идентификатор пользователя
        if ($_SESSION['role_id'] == "3") {
            return 0;
        } else{
            header("Location: ../index.php");
        }

    }
    public static function checkAdmin()
    {
        // Если сессия есть, вернем идентификатор пользователя
        if ($_SESSION['role_id'] == 2) {
            return 0;
        } else{
            header("Location: ../index.php");
        }

    }



    /**
     * Проверяет имя: не меньше, чем 6 символов
     * @param string $login <p>Пароль</p>
     * @return boolean <p>Результат выполнения метода</p>
     */
    public static function checkUserData($login)
    {

        // Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = 'SELECT * FROM users WHERE login = :login';

        // Получение результатов. Используется подготовленный запросw
        $result = $db->prepare($sql);
        $result->bindParam(':login', $login, PDO::PARAM_STR);
        $result->execute();
        return $result->fetch();
    }

    /**
     * Возвращает пользователя с указанным id
     * @param integer $id <p>id пользователя</p>
     * @return array <p>Массив с информацией о пользователе</p>
     */
    public static function getUserById($id)
    {
        // Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = 'SELECT * FROM user WHERE id = :id';

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        // Указываем, что хотим получить данные в виде массива
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();

        return $result->fetch();
    }

    public static function order_cart($id){
        $db = Db::getConnection();
        $sql = 'SELECT products_id FROM product_order WHERE id = :id ';
        $res = $db->prepare($sql);
        $res->bindParam(':id', $id, PDO::PARAM_INT);
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $res->execute();
        $cart = $res->fetch();
        $cart = unserialize(base64_decode($cart['products_id']));

        return $cart;
    }

    // Стирка
    public static function laundry_cart($stat){
        $db = Db::getConnection();
        $sql = 'SELECT products_id FROM laundry WHERE status = :stat';
        $res = $db->prepare($sql);
        $res->bindParam(':stat', $stat, PDO::PARAM_INT);
        $res->execute();
        $cart = $res->fetch();
        $cart = unserialize(base64_decode($cart['products_id']));
        return $cart ;
    }
    public static function laundry_cart_out($cart){
        $db = Db::getConnection();
        foreach ($cart as $key => $item) {
            $sql = $db->query("SELECT id,name,img,code,description FROM product WHERE id={$item['id']}");
            while ($result = $sql->fetch()) {
                echo '
				    	<tr>
							<td >
								<img src="' . $result['img'] . '" width="100px" height="150px" alt="">
							</td>
							<td>
								<p>' . $result["name"] . '</p>
							</td>
							<td>
                            <p>' . $cart[$key]['laundry'] . '</p>
							</td>
						</tr>
						    ';
            }
            continue;
            return;
        }
    }
    public static function laundry_count($cart){
       $count = 0;
        foreach ($cart as $key => $item) {
           $count += $item['laundry'];
        }
    return $count;
    }
    //Служебка
    public static function memo($id, $comment){
        $db = Db::getConnection();
        $sql = 'UPDATE product_order SET status_memo =?, memo_comment =? WHERE id =?';
        $db->prepare($sql)->execute([1,$comment,$id]);
        return 0;
    }

    //Токен
    public static function token($id){
        $db = Db::getConnection();
        if(isset($_SESSION['token'])){
            $token = $_SESSION['token'];
        }   else $token = null;
        $sql = 'UPDATE users SET token =? WHERE id =?';
        $db->prepare($sql)->execute([$token,$id]);
        return 0;
    }
    public static function check_token($id, $token){
     $db = Db::getConnection();
     $sql = 'SELECT token FROM users WHERE id = :id';
     $res  = $db->prepare($sql);
     $res->bindParam(':name', $id);
     $res->execute();
     $row= $res->fetch();
     if($row['token'] == $token){
      return 0;
     } else {
         session_start();
         session_destroy();
         header("Location:/index.php");
     }
 }
}
