<?php


class View
{
    public static function productscount($id){
        $db = Db::getConnection();
        $sql = 'SELECT id, name, description, img, availability, size_id FROM product WHERE subcategory_id = :id';
        $res = $db->prepare($sql);
        $res->execute(['id' => $id]);
        return $res->rowCount();
    }

    //Пагинация
    public static function pagination($id, $page = 1){
          $maxcount = View::productscount($id);
          $maxpage = ceil($maxcount/15);
          $pages = array();
          if($maxpage > 5 and $maxpage - $page >= 2 and $page <> 2 and $page <> 1){
              $pages[1] = $page - 2;
              $pages[2] = $page - 1;
              $pages[3] = $page;
              $pages[4] = $page + 1;
              $pages[5] = $page + 2;
              $pages['current'] = 3;
          }else if($maxpage > 5 and $maxpage - $page == 1){
              $pages[1] = $page - 3;
              $pages[2] = $page - 2;
              $pages[3] = $page - 1;
              $pages[4] = $page;
              $pages[5] = $page + 1;
              $pages['current'] = 4;
          }else if($maxpage > 5 and $maxpage == $page){
              $pages[1] = $page - 4;
              $pages[2] = $page - 3;
              $pages[3] = $page - 2;
              $pages[4] = $page - 1;
              $pages[5] = $page;
              $pages['current'] = 5;
          }else{
              if($maxpage >= 1) $pages[1] = 1;
              if($maxpage >= 2) $pages[2] = 2;
              if($maxpage >= 3) $pages[3] = 3;
              if($maxpage >= 4) $pages[4] = 4;
              if($maxpage >= 5) $pages[5] = 5;
              $pages['current'] = $page;
          }
          $minuspage = $page-1;
          $pluspage = $page+1;
          echo '
          <ul class="pagination">';
            if($page > 1) echo '<li><a href="?id='.$id.'&page=1">&laquo;</a></li> <li><a href="?id='.$id.'&page='. $minuspage .'"><</a></li>';
            if($maxpage >= 1): echo '<li '; echo $pages['current'] == 1 ? 'class="active"' : ''; echo '><a href="?id='.$id.'&page='.$pages[1].'">'. $pages[1] .'</a></li>';endif;
            if($maxpage >= 2): echo '<li '; echo $pages['current'] == 2 ? 'class="active"' : ''; echo '><a href="?id='.$id.'&page='.$pages[2].'">'. $pages[2] .'</a></li>';endif;
            if($maxpage >= 3): echo '<li '; echo $pages['current'] == 3 ? 'class="active"' : ''; echo '><a href="?id='.$id.'&page='.$pages[3].'">'. $pages[3] .'</a></li>';endif;
            if($maxpage >= 4): echo '<li '; echo $pages['current'] == 4 ? 'class="active"' : ''; echo '><a href="?id='.$id.'&page='.$pages[4].'">'. $pages[4] .'</a></li>';endif;
            if($maxpage >= 5): echo '<li '; echo $pages['current'] == 5 ? 'class="active"' : ''; echo '><a href="?id='.$id.'&page='.$pages[5].'">'. $pages[5] .'</a></li>';endif;
            if($page < $maxpage) echo '<li><a href="?id='.$id.'&page='. $pluspage .'">></a></li><li><a href="?id='.$id.'&page='. $maxpage .'">&raquo;</a></li>';
        '</ul>';
  }

  //Продукты
    public static function products($id, $page = 1)
 {
     $countonpage = 15;
     $offset = ($page-1) * $countonpage;
     $db = Db::getConnection();
     $sql = 'SELECT id, name, description, img, availability, size_id FROM product WHERE subcategory_id = :id LIMIT '.$offset.', '.$countonpage;
     $res = $db->prepare($sql);
     $res->execute(['id' => $id]);
     if ($res->rowCount()== 0){
         echo '
         <img src="/template/images/stich/stich1.gif" height="500" width="400px" alt="">
         <p>Таня и добавление товаров!!!</p>
         ';
     }
     else {
         while ($result = $res->fetch()) {
             $id = $result['size_id'];
          $sql = $db->query("SELECT name FROM size WHERE id = $id");
          $size = $sql->fetch();
             $count = '<a  name=' . $result["id"] . ' onClick="addToCart(' . $result["id"] . ')" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>В корзину</a>';
          if(isset($_SESSION['cart'])) {
              $cart = $_SESSION['cart'];
              foreach ($cart as $key => $item) {
                  if ($item['id'] == $result['id']) {
                      $count = '
                      <a  name=' . $result["id"] . ' class="btn btn-default add-to-cart" style="margin-left: 25%"><i class="fa fa-shopping-cart"></i>В корзине:</a>
                      <input class="cart_quantity_input" type="text" name="quantity" value="' . $_SESSION['cart'][$key]['count'] . '" oninput="count_cart(' . $result["id"] . ', this.value)" autocomplete="off" size="3" style = "float:right; margin-right: 14px">';
                  }
              continue;

              }
          }
             echo '
              <div class="col-sm-4">
                  <div class="product-image-wrapper">
                   <div class="single-products">
                   <div class="productinfo text-center">
                   <div class="image-container">
                <a href="product.php?id=' . $result['id'] . ' "><img src="' . $result['img'] . '"></a>
                   </div>
                   
                     <p>Размер: '.$size["name"].'</p><br>
                     <div class="hz" >
                     <h2>Доступно: ' . $result["availability"] . '</h2><br>  
                  <a  name=' . $result["id"] . ' href="product.php?id=' . $result['id'] . ' " class="btn btn-default add-to-cart"><i class="fa fa-info"></i>Описание</a><br>      
                 
                 '.$count.'
                 </div>
                 </div>
               </div>
            </div>
            </div>
                  ';
         }
     }

         return 0;
     }

    //Заказы
    public static function orders($id){

     $page = $_GET['page'] == "" ? 1 : $_GET['page'];
     $offset = ($page - 1) * 10;
     $db = Db::getConnection();
     $sql = 'SELECT * FROM product_order WHERE status != 3 AND status != 4 AND user_id = :id LIMIT 10 offset '.$offset;
     $res = $db->prepare($sql);
     $res->bindParam(':id', $id, PDO::PARAM_INT);
     $res->execute();
     while ($result = $res->fetch()) {
         $status = $result["status"];
         if($status == "1") $status1 = "Заказ принят";
         if($status == "2") $status1 = "Заказ готов к выдаче";
         if($status == "5") $status1 = "Заказ выдан";
             $id = $result['id'];
             echo "
             <tr>
             <td>{$result['id']}</td>
             <td>{$result['date']}</td>
             <td>{$result['user_comment']}</td>
             <td>{$status1}</td>
             <td>
             <a  href='order_cart.php?id=$id'>Посмотреть заказ |</a>
             <a href='/PhpWord/index.php?id=$id'>Скачать служебку</a>                                 
             </td>
             </tr>
         ";
     }
     return 0;
 }

    //Вывод корзины заказа
    public static function order_cart_out($cart){
     $db = Db::getConnection();
     foreach ($cart as $key => $item) {
         //$sql = $db->query('SELECT `name`, `img`, `code` FROM `product`  WHERE id == $item["id"]');
         $sql = $db->query("SELECT name,img, description FROM product WHERE id={$item['id']}");
         while ($result = $sql->fetch()) {
             echo '
             <tr>
             <td><img src="'.$result['img'].'" width="100px" height="150px"></td>
             <td><p>'.$result['name'].'</p></td>
             <td><p>'.$result['description'].'</p></td>
             <td><p>'.$cart[$key]['count'].'</p></td>
             </tr>
             ';
         }
         continue;
         return;
     }
 }

    //TODO уточнить ПЕРЕДЕЛАТЬ product.php
    public static function product($id){
        $db = Db::getConnection();
        $sql = 'SELECT id, name, description, availability, code, size_id FROM product WHERE id = :id';
        $res = $db->prepare($sql);
        $res->bindParam(':id', $id, PDO::PARAM_INT);
        $res->execute();

     return $res->fetch();

 }

   //Вывод картинок на стр продукта
    public static function img($id){
     $db = Db::getConnection();
     $sql = 'SELECT id, img FROM img WHERE  product_id = :id';
     $res = $db->prepare($sql);
     $res->bindParam(':id', $id, PDO::PARAM_INT);
     $res->execute();
     //$res->fetch();
     $i = 0;
     while ($result = $res->fetch()){
         if($i == 0) $hidden = ''; else $hidden = 'hidden';
         echo '<img id="gallery'.$i++.'" src="'.$result['img'].'" alt="" '.$hidden.' />';
     }
     echo '<script>var max = '.$i.';</script>';
     return 0;
 }

 //SQL таблица size (размер товаров)
 public static function size($id){
        $db = Db::getConnection();
        $sql =  $db->query('SELECT `id`, `name` FROM `size`  WHERE id = '.$id.'');

        return $sql->fetch();
    }

    //SQL таблица size (размер товаров)
    public static function size_id($id){
        $db = Db::getConnection();
        $sql = 'SELECT availability, size_id FROM product_size WHERE  product_id = :id';
        $res = $db->prepare($sql);
        $res->bindParam(':id', $id, PDO::PARAM_INT);
        $res->execute();
        $res->fetch();
        $result = $res->fetch();
        $id = $result['size_id'];
        $sql =  $db->query('SELECT `id`, `name` FROM `size`  WHERE id = '.$id.'');
        return 0;
    }

    //TODO уточнить
    public static function laundry($id,$page = 1){
        $countonpage = 15;
        $offset = ($page-1) * $countonpage;
        $db = Db::getConnection();
        $sql = 'SELECT id, name, description, img, laundry, size_id FROM product WHERE laundry != :id LIMIT '.$offset.', '.$countonpage;
        $res = $db->prepare($sql);
        $res->execute(['id' => $id]);
            while ($result = $res->fetch()) {
                $id = $result['size_id'];
                $sql = $db->query("SELECT name FROM size WHERE id = $id");
                $size = $sql->fetch();
                echo '
              <div class="col-sm-4">
                  <div class="product-image-wrapper">
                   <div class="single-products">
                   <div class="productinfo text-center">
                   <div class="image-container">
                   <img src="' . $result['img'] . '">
                   </div>
                     <p>Размер: '.$size["name"].'</p><br>
                     <div class="hz" >
                     <h2>Стирка: ' . $result["laundry"] . '</h2><br> 
                 </div>
                 </div>
               </div>
            </div>
            </div>
                  ';
            }


        return 0;
    }

    //TODO Убрать стич функция в процессе перреписать
    public static function product_out($id, $cat){
        if($cat == "2") $wer = 'category_id';
        if($cat == "3") $wer = 'subcategory_id';
        if($cat == "4") $wer = 'final_cat_id';
        $page = 1;
        $countonpage = 3;
        $offset = ($page-1) * $countonpage;
        $db = Db::getConnection();
        $sql = 'SELECT id, name, description, img, availability, size_id FROM product WHERE '.$wer.' = :id LIMIT '.$offset.', '.$countonpage;
        $res = $db->prepare($sql);
        $res->execute(['id' => $id]);
        while ($result = $res->fetch()) {
            $id = $result['size_id'];
            $sql = $db->query("SELECT name FROM size WHERE id = $id");
            $size = $sql->fetch();
            $count = '<a  name=' . $result["id"] . ' onClick="addToCart(' . $result["id"] . ')" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>В корзину</a>';
            if(isset($_SESSION['cart'])) {
                $cart = $_SESSION['cart'];
                foreach ($cart as $key => $item) {
                    if ($item['id'] == $result['id']) {
                        $count = '
                      <a  name=' . $result["id"] . ' class="btn btn-default add-to-cart" style="margin-left: 25%"><i class="fa fa-shopping-cart"></i>В корзине:</a>
                      <input class="cart_quantity_input" type="text" name="quantity" value="' . $_SESSION['cart'][$key]['count'] . '" oninput="count_cart(' . $result["id"] . ', this.value)" autocomplete="off" size="3" style = "float:right; margin-right: 14px">';
                    }
                    continue;

                }
            }
            echo '
              <div class="col-sm-4">
                  <div class="product-image-wrapper">
                   <div class="single-products">
                   <div class="productinfo text-center">
                   <div class="image-container">
                  <a href="product.php?id=' . $result['id'] . ' ">  <img src="' . $result['img'] . '"></a>
                   </div>
                     <p>Размер: '.$size["name"].'</p><br>
                     <div class="hz" >
                     <h2>Доступно: ' . $result["availability"] . '</h2><br>  
                  <a  name=' . $result["id"] . ' href="product.php?id=' . $result['id'] . ' " class="btn btn-default add-to-cart"><i class="fa fa-info"></i>Описание</a><br>      
                 
                 '.$count.'
                 </div>
                 </div>
               </div>
            </div>
            </div>
                  ';
        }


    }
    //Служебки подтверждение
    public static function memo_out(){
        $page = $_GET['page'] == "" ? 1 : $_GET['page'];
        $offset = ($page - 1) * 10;
        $db = Db::getConnection();
        $sql = 'SELECT * FROM product_order WHERE status != 3 AND status != 4 AND status != 5 AND status_memo != 1 LIMIT 10 offset '.$offset;
        $res = $db->prepare($sql);
        $res->bindParam(':id', $id, PDO::PARAM_INT);
        $res->execute();
        while ($result = $res->fetch()) {
                $status = $result["status"];
                $memo = $result["status_memo"];
                if ($status == "1") $status1 = "Заказ принят";
                if ($status == "2") $status1 = "Заказ готов к выдаче";
                if ($status == "5") $status1 = "Заказ выдан";
                //if ($status != "3" && $status != "4" && $memo != "1" && $status != "5") {
                    $id = $result['id'];
                    echo "
             <tr>
             <td>{$result['id']}</td>
             <td>{$result['date']}</td>
             <td>{$result['user_comment']}</td>
             <td>{$result['user_name']}</td>
             <td>
             <a href='?page={$_GET['page']}&comment=true&id={$result['id']}'>Подтвердить заказ</a>                                 
             </td>
             </tr>
         ";
              //  }
            }
        return 0;
    }


    public static function size_out(){
        $db = Db::getConnection();
        $sql = 'SELECT name FROM size WHERE id?';
        $res = $db->prepare($sql)->execute([$size_id]);
        return $res->fetch();
    }


 }
