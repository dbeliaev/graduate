<?php
session_start();
require __DIR__ . "/../components/Autoload.php";
User::checkLogged();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Cart | E-Shopper</title>
    <link href="../template/css/bootstrap.min.css" rel="stylesheet">
    <link href="../template/css/font-awesome.min.css" rel="stylesheet">
    <link href="../template/css/prettyPhoto.css" rel="stylesheet">
    <link href="../template/css/price-range.css" rel="stylesheet">
    <link href="../template/css/animate.css" rel="stylesheet">
	<link href="../template/css/main.css" rel="stylesheet">
	<link href="../template/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="../template/js/html5shiv.js"></script>
    <script src="template/js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="template/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="template/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="template/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="template/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="template/images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
<?php  
     include "html-files/header.php"; 
?> 

	<section id="cart_items">
		<div class="container" style="width: 1470px">

			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image"></td>
							<td class="description">Название</td>
							<td class="quantity">Количество</td>
							<td class="total">Доступно</td>
							<td></td>
						</tr>
					</thead>
                    <?php
                    Cart::out();
                    ?>
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->

	<section id="do_action">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div class="total_area">
                        <h2>Комментарий</h2>
                        <h6>!!!Обязательно укажите дату мероприятия под комментарием !!!</h6>
                        <form action="../controls/ord.php" method="post">
                        <textarea name="comment"  placeholder="Коментарий (обязательно)!!"></textarea>
                        <?php
                        if(isset($_GET['error'])) {
                            echo '<h4>'.$_GET['error'].'</h4>';
                        }
                        ?>
                            <input type="date" name="event_date" style="margin-right:225px;">
                        <button type="submit" class="btn btn-primary">Заказать</button>
                        </form>
					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->

	<?php  
     include "html-files/footer.php";
	?>

<script src="../template/js/jquery.js"></script>
<script src="../template/js/bootstrap.min.js"></script>
<script src="../template/js/jquery.scrollUp.min.js"></script>
<script src="../template/js/jquery.prettyPhoto.js"></script>
<script src="../template/js/main.js"></script>
<script src="../template/js/myCart.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="../template/js/search.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
</body>
</html>