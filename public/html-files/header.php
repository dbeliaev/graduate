<?php
session_start();
include "../components/Autoload.php";
if ($_SESSION['login'] == 'General'){
$orders = '<li><a href="memo.php?page=1"><i class="fa fa-user"></i>Служебки</a></li>';
}
if ($_SESSION['role_id'] == "3"){
    $cart = User::laundry_cart(1);
    $count = User::laundry_count($cart);
    $laundry = '<li><a href="cart_laundry.php"><i class="fa fa-refresh "></i>Стирка ('.$count.')</a></li>';
}
?>


<header id="header"><!--header-->
    <div class="header_top"><!--header_top-->
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="contactinfo">
                        <ul class="nav nav-pills">
                            <li><a href="#"><i class="fa fa-phone"></i> IP 1176</a></li>
                            <li><a href="#"><i class="fa fa-envelope"></i>  events_pk@mirea.ru</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="social-icons pull-right">
                        <ul class="nav navbar-nav">

                            <li><a href="https://vk.com/vector_mirea"><i class="fa fa-vk"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header_top-->
    
    <div class="header-middle"><!--header-middle-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="logo pull-left">
                        <a href="first.php"><img src="../template/images/home/logo.jpg" alt="Logo" /></a>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="shop-menu pull-right">
                        <ul class="nav navbar-nav">
                            <?php
                            echo $laundry;
                            echo $orders;
                            ?>
                            <li><a href="orders.php?page=1"><i class="fa fa-crosshairs"></i> Заказы</a></li>
                            <li><a href="cart.php"><i class="fa fa-shopping-cart"></i> Корзина</a></li>
                            <li><a href="../controls/session_dest.php?id=<?php $_SESSION['id'] ?>"><i class="fa fa-sign-out"></i>Выход</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-middle-->

    <div class="header-bottom"><!--header-bottom-->
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="mainmenu pull-left">
                        <ul class="nav navbar-nav collapse navbar-collapse">
                            <?php

                            if ($_SESSION['role_id'] == "3"){
                                Categories::header_cat(3);
                            }else{
                                Categories::header_cat(1);
                            }

                            ?>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div  class="search_box pull-right">
                        <input type="text" id = "search" name="referal" placeholder="Поиск" value="" class="who"  autocomplete="off"/>
                        <ul id="display" class="search_result">
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-bottom-->
</header><!--/header-->