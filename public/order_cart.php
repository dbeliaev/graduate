<?php
session_start();
require __DIR__ . "/../components/Autoload.php";
User::checkLogged();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Cart | E-Shopper</title>
    <link href="../template/css/bootstrap.min.css" rel="stylesheet">
    <link href="../template/css/font-awesome.min.css" rel="stylesheet">
    <link href="../template/css/prettyPhoto.css" rel="stylesheet">
    <link href="../template/css/price-range.css" rel="stylesheet">
    <link href="../template/css/animate.css" rel="stylesheet">
	<link href="../template/css/main.css" rel="stylesheet">
	<link href="../template/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="../template/js/html5shiv.js"></script>
    <script src="template/js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="template/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="template/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="template/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="template/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="template/images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
<?php
     include "html-files/header.php";
?>

	<section id="cart_items">
		<div class="container">

			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image"></td>
							<td class="description">Название</td>
                            <td class="description">Описание</td>
							<td class="quantity">Количество</td>
						</tr>
					</thead>
                    <tbody>
                    <?php

                  $cart = User::order_cart($_GET['id']);
                  View::order_cart_out($cart);
                  ?>
                    </tbody>

				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->
<section id="do_action">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <?php
                $db = Db::getConnection();
                $id = $_GET['id'];
                $sql = $db->query('SELECT `status` FROM `product_order`  WHERE id = '.$id.'');
                $result = $sql->fetch();
                $status =  $result['status'];
                if($status != "5"){
                    echo '<a class="btn btn-primary"  onclick="delorder('.$_GET['id'].')">Отменить заказ</a>';
                }else {
                    echo '<p>Заказ исполнен!</p>';
                }

                ?>
            </div>
        </div>
    </div>
</section><!--/#do_action-->
	<?php
     include "html-files/footer.php";
	?>

<script src="../template/js/jquery.js"></script>
<script src="../template/js/bootstrap.min.js"></script>
<script src="../template/js/jquery.scrollUp.min.js"></script>
<script src="../template/js/jquery.prettyPhoto.js"></script>
<script src="../template/js/main.js"></script>
<script src="../template/js/myCart.js?<?= $cssver ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
</body>
</html>