<?php
session_start();
require __DIR__ . "/../components/Autoload.php";
User::checkLogged();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Cart | E-Shopper</title>
    <link href="../template/css/bootstrap.min.css" rel="stylesheet">
    <link href="../template/css/font-awesome.min.css" rel="stylesheet">
    <link href="../template/css/prettyPhoto.css" rel="stylesheet">
    <link href="../template/css/price-range.css" rel="stylesheet">
    <link href="../template/css/animate.css" rel="stylesheet">
    <link href="../template/css/main.css" rel="stylesheet">
    <link href="../template/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="../template/js/html5shiv.js"></script>
    <script src="template/js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="template/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="template/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="template/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="template/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="template/images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
<?php
include "html-files/header.php";
?>

<section id="cart_items">
    <div class="container">

        <div class="table-responsive cart_info">
            <table class="table table-condensed">
                <thead>
                <tr class="cart_menu">
                    <td class="description">Номер заказа</td>
                    <td class="description">Дата заказа</td>
                    <td class="description">Kомментарий</td>
                    <td class="description">Статус</td>
                    <td class="description">Действия</td>
                </tr>
                </thead>
                <tbody>
                <?php
                 View::orders($_SESSION['id']);
                ?>
                </tbody>

            </table>
            <?php
            $db = Db::getConnection();
            $maxpage = ceil($db->query("select count(*) from product_order where user_id = {$_SESSION['id']}")->fetch()[0]/10);
            $page = $_GET['page'];
            ?>

            <ul class="">
                <?php if($page > 1): ?><li><a class="btn btn-primary"  href="?page=1">First</a></li>
                    <li><a class="btn btn-primary"  href="?page=<?= $_GET['page']-1 ?>">Previous</a></li><?php endif; ?>
                <?php if($page != $maxpage): ?><li><a class="btn btn-primary"  href="?page=<?= $_GET['page']+1 ?>">Next</a></li>
                    <li><a class="btn btn-primary"  href="?page=<?= $maxpage ?>">Last</a></li><?php endif; ?>
            </ul>
        </div>
    </div>
</section> <!--/#cart_items-->



<?php
include "html-files/footer.php";
?>

<script src="../template/js/jquery.js"></script>
<script src="../template/js/bootstrap.min.js"></script>
<script src="../template/js/jquery.scrollUp.min.js"></script>
<script src="../template/js/jquery.prettyPhoto.js"></script>
<script src="../template/js/main.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="../template/js/search.js"></script>
</body>
</html>