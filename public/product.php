<?php
include "../components/Autoload.php";
session_start();
User::checkLogged();
$id = $_GET['id'];
$result = View::product($id);
$size = View::size($result["size_id"]);
if($result["size_id"] == '1') $size_out = '<p>Размер:'.$size["name"].'</p>';
if($result["size_id"] != '1') {
$db = Db::getConnection();
    $sql = 'SELECT id, size_id FROM product WHERE name = :name';
    $res = $db->prepare($sql);
    $res->bindParam(':name', $result['name']);
    $res->execute();
    $size_out = '<p>Размер: <select>';
    while ($size_id = $res->fetch()){
        $sql2 = 'SELECT id, name FROM size WHERE id = :id';
        $res2 = $db->prepare($sql2);
        $res2->bindParam(':id', $size_id['size_id']);
        $res2->execute();
        while($size_name = $res2->fetch()){
            $tmp = $result['size_id'] == $size_name['id'] ? 'selected' : '';
            $size_out .= $size_name['id'];
            $size_out .= '
            <option  onclick="window.location.href=\'/public/product.php?id='.$size_id["id"].' \' "  value='.$size_name['id'].' '.$tmp.'>'.$size_name['name'].'</option>
            ';
        }
    }
    $size_out .= '</select></p>';


}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php echo $result['name'];?></title>
    <link href="../template/css/bootstrap.min.css" rel="stylesheet">
    <link href="../template/css/font-awesome.min.css" rel="stylesheet">
    <link href="../template/css/prettyPhoto.css" rel="stylesheet">
    <link href="../template/css/price-range.css" rel="stylesheet">
    <link href="../template/css/animate.css" rel="stylesheet">
    <link href="../template/css/main.css" rel="stylesheet">
    <link href="../template/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="../template/js/html5shiv.js"></script>
    <script src="../template/js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="../template/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../template/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../template/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../template/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../template/images/ico/apple-touch-icon-57-precomposed.png">
</head>

<body>
<?php
include "html-files/header.php";
?>


	<section>
		<div class="container">
			<div class="row">
				
				<div class="col-sm-9 padding-right">
					<div class="product-details"><!--product-details-->
						<div class="col-sm-1"><i class="fa fa-arrow-left" onclick="if(now == 0){now = max-1; document.getElementById('gallery0').setAttribute('hidden', true); document.getElementById('gallery' + now).removeAttribute('hidden');} else{now--;document.getElementById('gallery' + (now+1)).setAttribute('hidden', true); document.getElementById('gallery' + now).removeAttribute('hidden');}"></i></div>
                        <div class="col-sm-5">
							<div class="view-product">

							<?php
                           // echo '<img src="'.$img.'" alt="" />';
                            View::img($id);
                            ?>
                            <script> var now = 0; </script>
                            </div>

						</div>
                        <div class="col-sm-1"><i class="fa fa-arrow-right" onclick="if(document.getElementById('gallery' + (now+1)) != null){now++;document.getElementById('gallery' + (now-1)).setAttribute('hidden', true); document.getElementById('gallery' + now).removeAttribute('hidden');}else{document.getElementById('gallery' + now).setAttribute('hidden', true); now=0; document.getElementById('gallery0').removeAttribute('hidden');}"></i></div>
						<div class="col-sm-5">
							<div class="product-information"><!--/product-information-->
                             <?php  echo ' <h2>'.$result["name"].'</h2>
                                          <p>Web ID: '.$result["code"].'</p>
                                         <span>
									<span><h2 style="color: orange">Доступно: ' .$result["availability"]. '</h2></span>
									<a  name=' . $result["id"] . ' onClick="addToCart(' . $result["id"] . ')" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>В корзину</a>
								</span>
								'.$size_out.'
                                       '; ?>

							</div><!--/product-information-->
						</div>
					</div><!--/product-details-->
					
					<div class="category-tab shop-details-tab"><!--category-tab-->
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#reviews" data-toggle="tab">Описание</a></li>
							</ul>
						</div>
						<div class="tab-content">
							
							<div class="tab-pane fade active in" id="reviews" >
								<div class="col-sm-12">
									<?php echo '<p> '.$result["description"].' </p>';?>
								</div>
							</div>
							
						</div>
					</div><!--/category-tab-->
					
				</div>
			</div>
		</div>
	</section>

<?php
include "html-files/footer.php";
?>

<script src="../template/js/jquery.js"></script>
<script src="../template/js/bootstrap.min.js"></script>
<script src="../template/js/jquery.scrollUp.min.js"></script>
<script src="../template/js/jquery.prettyPhoto.js"></script>
<script src="../template/js/main.js"></script>
<script src="../template/js/myCart.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="../template/js/search.js"></script>

</body>
</html>