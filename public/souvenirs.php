<?php
include "../components/Autoload.php";
session_start();
User::checkLogged();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Cart | E-Shopper</title>
    <link href="../template/css/bootstrap.min.css" rel="stylesheet">
    <link href="../template/css/font-awesome.min.css" rel="stylesheet">
    <link href="../template/css/prettyPhoto.css" rel="stylesheet">
    <link href="../template/css/price-range.css" rel="stylesheet">
    <link href="../template/css/animate.css" rel="stylesheet">
    <link href="../template/css/main.css" rel="stylesheet">
    <link href="../template/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="../template/js/html5shiv.js"></script>
    <script src="../template/js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="../template/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../template/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../template/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../template/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../template/images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
<?php
include "html-files/header.php";
?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Category</h2>
                    <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <?php
                                    $db = Db::getConnection();
                                    $sql = 'SELECT id,name, href_us FROM subcategory WHERE category_id = :id';
                                    $res = $db->prepare($sql);
                                    $res->bindParam(':id', $id = 6, PDO::PARAM_INT);
                                    $res->execute();
                                    while ($result = $res->fetch()) {
                                        $id = $result['id'];
                                        echo "<li><a href='souvenirs.php?id= $id'> {$result['name']}</a> </li>";
                                    }
                                    ?>
                                </h4>
                            </div>

                        </div>

                    </div><!--/category-products-->

                </div>
            </div>


            <div  class="col-sm-9 padding-right">
                <div id="search_out" class="features_items"><!--features_items-->
                    <h2 class="title text-center">Сувениры</h2>
                    <?php
                    if($_GET['page'] == '') $page = 1; else $page = intval($_GET['page']);
                    View::products($_GET['id'], $page);
                    ?>

                </div><!--features_items-->
                <?php View::pagination($_GET['id'], $page); ?>
            </div>
        </div>
    </div>
</section>


<?php
include "html-files/footer.php";
?>

<script src="../template/js/jquery.js"></script>
<script src="../template/js/bootstrap.min.js"></script>
<script src="../template/js/jquery.scrollUp.min.js"></script>
<script src="../template/js/jquery.prettyPhoto.js"></script>
<script src="../template/js/main.js"></script>
<script src="../template/js/myCart.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="../template/js/search.js"></script>

</body>
</html>

