<?php
include "../components/Autoload.php";
session_start();
User::checkVip();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Cart | E-Shopper</title>
    <link href="../template/css/bootstrap.min.css" rel="stylesheet">
    <link href="../template/css/font-awesome.min.css" rel="stylesheet">
    <link href="../template/css/prettyPhoto.css" rel="stylesheet">
    <link href="../template/css/price-range.css" rel="stylesheet">
    <link href="../template/css/animate.css" rel="stylesheet">
    <link href="../template/css/main.css" rel="stylesheet">
    <link href="../template/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="../template/js/html5shiv.js"></script>
    <script src="../template/js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="../template/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../template/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../template/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../template/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../template/images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
<?php
include "html-files/header.php";
?>
<section>
    <div class="container">
        <div class="row">

            <div  class="col-sm-9 padding-right">
                <div id="search_out" class="features_items"><!--features_items-->
                    <h2 class="title text-center">Сувениры элитные</h2>
                    <?php
                    if($_GET['page'] == '') $page = 1; else $page = intval($_GET['page']);
                    View::products(35, $page);
                    ?>

                </div><!--features_items-->
                <?php View::pagination(35, $page); ?>
            </div>
        </div>
    </div>
</section>


<?php
include "html-files/footer.php";
?>

<script src="../template/js/jquery.js"></script>
<script src="../template/js/bootstrap.min.js"></script>
<script src="../template/js/jquery.scrollUp.min.js"></script>
<script src="../template/js/jquery.prettyPhoto.js"></script>
<script src="../template/js/main.js"></script>
<script src="../template/js/myCart.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="../template/js/search.js"></script>

</body>
</html>


