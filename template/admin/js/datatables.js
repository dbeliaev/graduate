function tableSearch() {
    var phrase = document.getElementById('search-text');
    var table = document.getElementById('product');
    var regPhrase = new RegExp(phrase.value, 'i');
    var flag = false;
    for (var i = 1; i < table.rows.length; i++) {
        flag = false;
        for (var j = table.rows[i].cells.length - 1; j >= 0; j--) {
            flag = regPhrase.test(table.rows[i].cells[j].innerHTML);
            if (flag) break;
        }
        if (flag) {
            table.rows[i].style.display = "";
        } else {
            table.rows[i].style.display = "none";
        }

    }
}

//удаление из бд
function deldata(id){
    Swal.fire({
        title: 'Вы уверены?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Отмена',
        confirmButtonText: 'Удалить!'
    }).then((result) => {
        if (result.isConfirmed) {
          delete_from_data(id);
        }
    })
}
function delete_from_data(id){
    console.log('del' + id);
    $.ajax({
        async: false,
        type: "POST",
        url: "../../../AdminLTE/ajax/del_product.php",
        dataType:"text",
        data: 'action=add&id=' + id,
        error: function () {
            alert( "Не смог" );
        },
        success: function (response) {
            alert('Удалили');
        }
    });
}

//изменение записи
function edit_data(id){
    console.log('ed' + id);
    $.ajax({
        async: false,
        type: "POST",
        url: "../../../AdminLTE/ajax/data.php",
        dataType:"text",
        data: 'action=add&id=' + id,
        error: function () {
            alert( "Не смог" );
        },
        success: function (response) {
            alert('изменеили');
        }
    });
}

//удаление заказа
function delete_from_order(id){
    Swal.fire({
        title: 'Вы уверены?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Отмена',
        confirmButtonText: 'Удалить!'
    }).then((result) => {
        if (result.isConfirmed) {
            delete_from_order1(id);
        }
    })
}
function delete_from_order1(id){
    console.log('del' + id);
    $.ajax({
        async: false,
        type: "POST",
        url: "../../../AdminLTE/ajax/del_order.php",
        dataType:"text",
        data: 'action=add&id=' + id,
        error: function () {
            alert( "Не смог" );
        },
        success: function (response) {
            window.location.reload(true);
        }
    });
}

//возврат со стиркой
function RetFromOrderLaundry(id,or_id) {
    const {value: formValues} = Swal.fire({
        title: 'Укажите количество',
        html:
            '<input id="count" class="swal2-input" placeholder="Кол-во">' +
            '<input id="laundry"  class="swal2-input" placeholder="Стирка">',
        focusConfirm: false,
        showCancelButton: true,
        confirmButtonText: 'Вернуть',
        cancelButtonText: 'Отмена',
        preConfirm: () => {
            retFromOrderLaundry(id, or_id,document.getElementById('count').value,  document.getElementById('laundry').value);
        }
    })
}
function retFromOrderLaundry(id, or_id, count, laundry){
    console.log('ret' + id);
    $.ajax({
        async: false,
        type: "POST",
        url: "../../../AdminLTE/ajax/retFromOrderLaundry.php",
        dataType:"text",
        data:{"id": id,"or_id": or_id,"count": count, "laundry": laundry},
        error: function () {
            alert( "Не смог" );
        },
        success: function (response) {
            //alert('Вернули');
        }
    });
}

//возврат товара
function RetFromOrder(id , or_id){
    Swal.fire({
        title: 'Укажите количество!',
        input: 'text',
        inputAttributes: {
            autocapitalize: 'off'
        },
        showCancelButton: true,
        confirmButtonText: 'Вернуть',
        cancelButtonText: 'Отмена',
        showLoaderOnConfirm: true,
        preConfirm: (count) => {
            retFromOrder(id,count,or_id);
        }
    })
}
function retFromOrder(id, count, or_id){
    console.log('ret' + id);
    $.ajax({
        async: false,
        type: "POST",
        url: "../../../AdminLTE/ajax/retFromOrder.php",
        dataType:"text",
        //data: 'action=add&id=' + id,
        data:{"id": id,"count": count,"or_id": or_id},
        //data: 'action=add&count=' + count,
        error: function () {
            alert( "Не смог" );
        },
        success: function (response) {
            alert('Вернули');
        }
    });
}

//Статус стирки
function laundry_status(id){
    Swal.fire({
        title: 'Вы уверены?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Отмена',
        confirmButtonText: 'Да!'
    }).then((result) => {
        if (result.isConfirmed) {
            landry_st(id);
        }
    })
}
function landry_st(id){
    console.log('ret' + id);
    $.ajax({
        async: false,
        type: "POST",
        url: "../../../AdminLTE/ajax/laundry_stat.php",
        dataType:"text",
        //data: 'action=add&id=' + id,
        data:{"id": id},
        //data: 'action=add&count=' + count,
        error: function () {
            alert( "Не смог" );
        },
        success: function (response) {
            window.location.reload(true);
        }
    });
}


//Заказ собран
function order_status(id){
    Swal.fire({
        title: 'Вы уверены?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Отмена',
        confirmButtonText: 'Да!'
    }).then((result) => {
        if (result.isConfirmed) {
            order_status1(id);
        }
    })
}
function order_status1(id){
    console.log('del' + id);
    $.ajax({
        async: false,
        type: "POST",
        url: "../../../AdminLTE/ajax/status.php",
        dataType:"text",
        data: 'action=add&id=' + id,
        error: function () {
            alert( "Не смог" );
        },
        success: function (response) {
            window.location.reload(true);
        }
    })
}

//Заказ собран
function order_suc(id){
    Swal.fire({
        title: 'Вы уверены?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Отмена',
        confirmButtonText: 'Да!'
    }).then((result) => {
        if (result.isConfirmed) {
            order_suc1(id);
        }
    })
}
function order_suc1(id){
    console.log('del' + id);
    $.ajax({
        async: false,
        type: "POST",
        url: "../../../AdminLTE/ajax/order_succes.php",
        dataType:"text",
        data: 'action=add&id=' + id,
        error: function () {
            alert( "Не смог" );
        },
        success: function (response) {
           window.location.reload(true);
        }
    })
}

//Заказ отменён
function order_stat_clance(id){
    Swal.fire({
        title: 'Вы уверены?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Отмена',
        confirmButtonText: 'Удалить!'
    }).then((result) => {
        if (result.isConfirmed) {
            order_stat_clance1(id);
        }
    })
}
function order_stat_clance1(id){
    console.log('del' + id);
    $.ajax({
        async: false,
        type: "POST",
        url: "../../../AdminLTE/ajax/order_clance.php",
        dataType:"text",
        data: 'action=add&id=' + id,
        error: function () {
            alert( "Не смог" );
        },
        success: function (response) {
            window.location.reload(true);
        }
    })
}

//удаление из архива
function delarc(id){
    Swal.fire({
        title: 'Вы уверены?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Отмена',
        confirmButtonText: 'Удалить!'
    }).then((result) => {
        if (result.isConfirmed) {
          delete_from_arc(id);
        }
    })
}
function delete_from_arc(id){
    console.log('del' + id);
    $.ajax({
        async: false,
        type: "POST",
        url: "../../../AdminLTE/ajax/arc_del.php",
        dataType:"text",
        data: 'action=add&id=' + id,
        error: function () {
            alert( "Не смог" );
        },
        success: function (response) {
            window.location.reload(true);
        }
    })
}

//Пароль Vk
function psswd_prepare(id){
    Swal.fire({
        title: 'Введите пароль!',
        input: 'text',
        inputAttributes: {
            autocapitalize: 'off'
        },
        showCancelButton: true,
        confirmButtonText: 'Ок',
        cancelButtonText: 'Отмена',
        showLoaderOnConfirm: true,
        preConfirm: (password) => {
            psswd(id,password);
        }

    })
}

function psswd(id, password){
    console.log('password' + id);
    $.ajax({
        async: false,
        type: "POST",
        url: "../../../AdminLTE/ajax/psswd.php",
        dataType:"text",
        data:{"id": id,"password": password},
        error: function () {
            alert( "Не смог" );
        },
        success: function (response) {
            window.location.href = response;
        }
    })
}