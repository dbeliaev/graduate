function selector(cat, id){
    $.ajax({
        async: false,
        type: "POST",
        url: "../../../AdminLTE/ajax/cat_admin.php",
        dataType:"text",
        data: 'cat=' + cat + '&id=' + id,
        error: function () {
            alert( "Не смог" );
        },
        success: function (response) {
            switch(cat){
                case 'header_cat': $("#category").html(response).show(); $('#category').trigger('change'); break;
                case 'category': $("#subcategory").html(response).show(); $('#subcategory').trigger('change'); break;
                case 'subcategory': $("#final_cat").html(response).show(); $('#final_cat').trigger('change'); break;
            }
        }
    })
}