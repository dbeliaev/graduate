
//Заказ
function delorder(id){
    Swal.fire({
        title: 'Вы уверены?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Отмена',
        confirmButtonText: 'Удалить!'
    }).then((result) => {
        if (result.isConfirmed) {
          deletOrder(id);
        }
    })
}
function deletOrder(id){
    console.log('del ' + id);
    $.ajax({
        async: false,
        type: "POST",
        url: "../../public/ajax/deletOrder.php",
        data: 'action=del&id=' + id,
        dataType:"text",
        error: function () {
            console.log('deleterror');
        },
        success: function (response) {
            if(response == 'ok') window.location.href = '/public/orders.php';
        }
    });
}
//Служебка
function memoPrepare(id){
    Swal.fire({
        title: 'Вы уверены?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Отмена',
        confirmButtonText: 'Подтвердить!'
    }).then((result) => {
        if (result.isConfirmed) {
            memo(id);
        }
    })
}

function memo(id){
    console.log('del ' + id);
    $.ajax({
        async: false,
        type: "POST",
        url: "../../public/ajax/memo.php",
        data: 'action=del&id=' + id + '&comment=' + document.getElementById('comment').value,
        dataType:"text",
        error: function () {
            console.log('memoerror');
        },
        success: function (response) {
            if(response == 'ok') window.location.href = '/public/memo.php?page=1';
        }
    });
}
//Корзина
function count_cart(id , count){
    console.log('del ' + id);
    $.ajax({
        async: false,
        type: "POST",
        url: "../../public/ajax/count_cart.php",
        data: 'action=del&id=' + id + '&count=' + count,
        dataType:"text",
        error: function () {
            console.log('deleterror');
        }
    });
}

function delFromCart(id){
    Swal.fire({
        title: 'Вы уверены?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Отмена',
        confirmButtonText: 'Удалить!'
    }).then((result) => {
        if (result.isConfirmed) {
            delCart(id);
        }
    })
}

function delCart(id){
    console.log('del ' + id);
    $.ajax({
        async: false,
        type: "POST",
        url: "../../public/ajax/RemoveFromCart.php",
        data: 'action=del&id=' + id,
        dataType:"text",
        error: function () {
        },
        success: function (response) {
            window.location.reload(true);
        }
    });
}

function addToCart(id){
    console.log('add ' + id);
    $.ajax({
        async: false,
        type: "POST",
        url: "../../public/ajax/Cart.php",
        dataType:"text",
        data: 'action=add&id=' + id,
        error: function () {
            alert( "Не смог" );
        },
        success: function (response) {
            window.location.reload(true);
        }
    });
}
function minusCart(id){
    console.log('add ' + id);
    $.ajax({
        async: false,
        type: "POST",
        url: "../../public/ajax/minusCart.php",
        dataType:"text",
        data: 'action=add&id=' + id,
        error: function () {
            alert( "Не смог" );
        },
        success: function (response) {
            alert('Удалили');
        }
    });
}